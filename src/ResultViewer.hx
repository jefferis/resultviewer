# Amira-Script-Object V3.0
# Coordinate viewing of GHT results
# Set a root directory for the registration
# and a keylist file containing the filestems without the _01.nrrd suffix
# and you should be set.

set myScriptDir ${SCRIPTDIR}

$this proc constructor { } {
    global this
    global myScriptDir
    echo "Script directory is: $myScriptDir"
    $this newPortFilename RegRootDir
    # Load directory mode
    $this RegRootDir setMode 3 
    $this RegRootDir setValue "/Users/jefferis/projects/ChiangReanalysis/ChiangReg/Registration.GHT2"
    
    $this newPortFilename KeyListFile
    set regroot [$this RegRootDir getValue]
    $this KeyListFile setValue "$regroot/keylist.txt"
    
    $this newPortText "RegType" 
    $this RegType setValue "9dof"
    $this newPortText "RefBrain" 
    $this RefBrain setValue "FCWB"
    
    $this newPortButtonList Action 3
    $this Action setLabel Action:
    $this Action setLabel 0 LoadSelected
    $this Action setLabel 1 SaveSelected
    $this Action setLabel 2 ShowSelected

    $this newPortButtonList Registration 3
    $this Registration setLabel Registration:
    $this Registration setLabel 0 SetupReg
    $this Registration setLabel 1 SaveReg
    $this Registration setLabel 2 Cleanup
    
    $this newPortIntSlider BrainNum
    $this BrainNum setMinMax 0 0
    $this BrainNum setValue 0
    
    $this newPortInfo BrainName

    $this setVar _numBrains 0
    $this setVar _brainList {}
    $this setVar _selectedBrains {}
    $this setVar _refbrain ""

    $this newPortToggleList Selected 1
    $this Selected setLabel "Selected"
    $this Selected setValue 0 0

    $this newPortToggleList SaveRegistrationAsCMTK 1
    $this Selected setValue 0 0
    
    # $this newPortInfo dirName
    
    # Try to load python stuff if available
    # set usePython [expr [source $myScriptDir/import.hx] == 0]
    # turn off python by default
    set usePython 0
    $this setVar _usePython $usePython
    # echo "usePython = $usePython"
    # Load refbrain if not using python
    if { ! $usePython } {
        $this loadRefBrain
    }
}

$this proc compute {} {
    set numBrains [$this getVar _numBrains]
    if { [ $this RegRootDir isNew ] } {
        cd [$this RegRootDir getFilename]
    }
    if { [ $this KeyListFile isNew ] } {
        $this loadKeyFile
        $this initialiseSelection
    }
    if { [ $this BrainNum isNew] && $numBrains } {
        $this show
        $this updateSelectionBox
    }
    if { [ $this Selected isNew] && $numBrains } {
        $this updateSelection
    }
    if { [ $this RefBrain isNew] } {
        $this loadRefBrain
    }
    if { [ $this Action isNew] } {
        if {[$this Action getValue] == 0} {
            $this loadSelectedBrainList
        }
        if {[$this Action getValue] == 1} {
            $this saveSelectedBrainList
        }
        if {[$this Action getValue] == 2} {
            echo [$this makeSelectedBrainList]
        }
        
    }
    if { [ $this Registration isNew] } {
        if {[$this Registration getValue] == 0} {
            $this setupRegistration
        }
        if {[$this Registration getValue] == 1} {
            $this saveRegistration
        }
        if {[$this Registration getValue] == 2} {
            $this cleanupRegistration
        }
        
    }
}

$this proc show {} {
    if { [$this getVar _usePython] } {
        set i [ $this BrainNum getValue ]
        Python exec "view = ght.show(keys\[$i\], refbrainsurf='refbrain/FC6475.surf')"
    } else {
        $this loadCurrentBrain
        $this applyTransformForCurrentBrainToRef
    }
    $this BrainName setValue [$this currentBrainName]
    viewer 0 redraw
}

$this proc currentBrain {} {
    $this BrainNum getValue
}

$this proc currentBrainName {} {
    set i [ $this currentBrain ]
    lindex [$this getVar _brainList] $i
}

$this proc loadKeyFile {} {
    set filename [$this KeyListFile getFilename]
    if { [file exists $filename]} {
        echo "Key file $filename exists!"
        
        set brainList [$this loadBrainList $filename]
        set numBrains [llength $brainList]

        if { $numBrains > 0} {
            $this BrainNum setMinMax 0 [expr $numBrains - 1]
            $this setVar _numBrains $numBrains
            $this setVar _brainList $brainList
            # Load keylist in python as well
            if { [$this getVar _usePython] } {
                Python exec "keys = ght.loadKeys('$filename')"
            }
            $this initialiseSelection
        }
    }
}

$this proc makeAnimate {} {
    create HxDynamicParameter {Animate}
    Animate object connect $this
    Animate time setMinMax 0 [$this BrainNum getMaxValue]
    Animate time setSubMinMax 0 [$this BrainNum getMaxValue]
    # Animate time setValue 62
    Animate time setDiscrete 1
    Animate time setIncrement 1
    Animate time animationMode -once
    Animate fire
    Animate port setIndex 0 0
    Animate fire
    Animate value setState {t}
    Animate time setValue [$this BrainNum getValue]
    Animate fire
    Animate setViewerMask 16383
    Animate select
    Animate setPickable 1
}

# _selected Brains is a list of 1s and 0s
$this proc updateSelection {} {
    set selectedBrains [$this getVar _selectedBrains]
    set cb [$this currentBrain]
    set boxstate [$this Selected getValue 0]
    # echo "cb = $cb and boxstate = $boxstate"
    lset selectedBrains $cb $boxstate
    $this setVar _selectedBrains $selectedBrains
}

$this proc updateSelectionBox {} {
    set selectedTF [lindex [$this getVar _selectedBrains] [$this currentBrain]]
    $this Selected setValue 0 $selectedTF
}

$this proc initialiseSelection {} {
    echo "Initialising selection!"
    set numBrains [$this getVar _numBrains]
    set out {}
    for {set i 0} {$i < $numBrains} {incr i} {lappend out 0}
    $this setVar _selectedBrains $out
}

$this proc makeSelectedBrainList {} {
    set numBrains [$this getVar _numBrains]
    set selectedBrains [$this getVar _selectedBrains]
    set brainList [$this getVar _brainList]
    
    set out {}
    for {set i 0} {$i < $numBrains} {incr i} {
        if { [lindex $selectedBrains $i] > 0} {
            # append ith brain name
            set cb [lindex $brainList $i]
            lappend out $cb
        }
    }
    return $out
}

$this proc saveSelectedBrainList {} {
    set selbrains [$this makeSelectedBrainList]

    set klf [$this KeyListFile getValue]
    set klfroot [file rootname $klf]
    set sklf "$klfroot-sel.txt"
    # if selection file already exists, move existing file aside as a backup
    if { [file exists $sklf] } {
        file rename -force $sklf "$sklf.bak"
    }
    $this writeTextFile $sklf $selbrains
}

$this proc writeTextFile { filename text } {
    set fileId [open $filename "w"]
    
    foreach line $text {
        echo $line
        puts $fileId $line
    }
    close $fileId
}

$this proc loadSelectedBrainList {} {
    set klf [$this KeyListFile getFilename]
    set klfroot [file rootname $klf]
    set sklf "$klfroot-sel.txt"
    if { [file exists $sklf]} {
        echo "Loading selection file $sklf!"
        set selectedBrainList [$this loadBrainList $sklf]
        set numBrains [llength $selectedBrainList]
        
        if { $numBrains > 0} {
            # set selection list to all 1s
            $this initialiseSelection
            set brainList [$this getVar _brainList]
            set selectedBrains [$this getVar _selectedBrains]
            
            foreach brain $selectedBrainList {
                set idx [lsearch -exact $brainList $brain]
                if { $idx > -1 } {
                    lset selectedBrains $idx 1
                } else {
                    echo "Couldn't find $brain amongst loaded brains"
                }
            }
            
            $this setVar _selectedBrains $selectedBrains
        }
    }
}

$this proc loadBrainList { filename } {
    # load generic text file, returning list with non-empty lines
    # that do not begin with a hash
    set brainList {}
    if { [ catch {set fp [open $filename r]}]} {
        echo "unable to open $filename"
        return $brainList
    }
    set file_data [read $fp]
    close $fp
    set data [split $file_data "\n"]

    # include non-empty lines that do not begin with #
    foreach line $data {
        if {[string length $line] > 0 && 
            [string compare -length 1 $line "#"] != 0 } {
                lappend brainList $line
            }
    }
    return $brainList
}

$this proc dirForCurrentBrain { } {
	set brainkey [$this currentBrainName]
	set regRoot [$this RegRootDir getValue]
	set refbrain [$this RefBrain getValue]
	set chan "01"
	set regtype [$this RegType getValue]
	set regname "${refbrain}_${brainkey}_${chan}_${regtype}.list"
	set dirname [file join $regRoot "affine" $regname]
	return $dirname 
}

$this proc loadTransformForCurrentBrain { } {
	set dirname [$this dirForCurrentBrain]
	set filename [file join $dirname "hxtransform"]
	$this loadTransform $filename
}

$this proc applyTransformForCurrentBrainToRef { } {
    set xform [$this loadTransformForCurrentBrain]
    $this applyTransformToRef $xform
}

$this proc applyTransformToRef { l } {
	set refbrain [$this getVar _refbrain]
    # echo "transform is $l"
    #$refbrain setTransform [lindex $l 0 ] [lindex $l 4 ] [lindex $l 8 ] [lindex $l 12 ] [lindex $l 1 ] [lindex $l 5 ] [lindex $l 9 ] [lindex $l 13 ] [lindex $l 2 ] [lindex $l 6 ] [lindex $l 10 ] [lindex $l 14 ] [lindex $l 3 ] [lindex $l 7 ] [lindex $l 11 ] [lindex $l 15 ]
    $refbrain setTransform [lindex $l 0] [lindex $l 1] [lindex $l 2] [lindex $l 3] [lindex $l 4] [lindex $l 5] [lindex $l 6] [lindex $l 7] [lindex $l 8] [lindex $l 9] [lindex $l 10] [lindex $l 11] [lindex $l 12] [lindex $l 13] [lindex $l 14] [lindex $l 15]
}

$this proc loadTransform { filename } {
	set l [$this loadBrainList $filename]
	set out { 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 }
	if { [llength $l] == 1 } {
        # set out [lindex $l 0]
        set out [lindex $l 0]
	}
	if { [llength $out] > 1 } {
        # permute order assuming that they have been written row-wise (whereas Amira expects colwise)
        set out [concat [lindex $out 0 ] [lindex $out 4 ] [lindex $out 8 ] [lindex $out 12 ] [lindex $out 1 ] [lindex $out 5 ] [lindex $out 9 ] [lindex $out 13 ] [lindex $out 2 ] [lindex $out 6 ] [lindex $out 10 ] [lindex $out 14 ] [lindex $out 3 ] [lindex $out 7 ] [lindex $out 11 ] [lindex $out 15 ]]
	}
	return $out
}

$this proc permutexform { xform } {
    concat [lindex $xform 0 ] [lindex $xform 4 ] [lindex $xform 8 ] [lindex $xform 12 ] [lindex $xform 1 ] [lindex $xform 5 ] [lindex $xform 9 ] [lindex $xform 13 ] [lindex $xform 2 ] [lindex $xform 6 ] [lindex $xform 10 ] [lindex $xform 14 ] [lindex $xform 3 ] [lindex $xform 7 ] [lindex $xform 11 ] [lindex $xform 15 ]
}

$this proc loadRefBrain { } {
	set refbrain [$this getVar _refbrain]
    # First clean up any existing brains
    if {[exists refbrainsurfview] } {
		remove refbrainsurfview
	}
    if {[exists $refbrain] } {
		remove $refbrain
	}
	
    global myScriptDir
    set refbrain [$this RefBrain getValue]
    set refbrainsurf "${refbrain}.surf"
    set refbrainpath [file join [file dirname $myScriptDir] "hx/${refbrainsurf}"]
    if {[exists $refbrainsurf] } {
		remove $refbrainsurf
	}
	echo "Loading ref brain $refbrainsurf"
	set refbrain [load $refbrainpath]
	create HxDisplaySurface {refbrainsurfview}
	refbrainsurfview data connect $refbrain
	refbrainsurfview colormap setDefaultColor 1 0.1 0.1
	refbrainsurfview colormap setDefaultAlpha 0.500000
	refbrainsurfview colormap setLocalRange 0
	refbrainsurfview fire
	set LINES 2
	refbrainsurfview drawStyle setValue $LINES
	refbrainsurfview fire
	$this setVar _refbrain $refbrain
}

$this proc makeVoltex { } {
	if {[exists "curbrainview"]} {
		curbrainview data disconnect
	} else {
		create HxVoltex {curbrainview}
		curbrainview colormap setDefaultColor 1 0.1 0.1
		curbrainview colormap setDefaultAlpha 0.500000
		curbrainview texture2D3D setValue 1
		curbrainview slices setValue 256
		curbrainview colormap setLocalRange 0
		curbrainview fire
	}
}

$this proc loadCurrentBrain { } {
	set dirname [$this dirForCurrentBrain]
	# standard path to low res version of original (unregistered) sample image
	set filename [file join $dirname "lowres.am"]

	# figure out possible path to alternative version of this image
	# which will be e.g.
	# ChiangReg/images4um/5HT1bMARCM-F000001_seg001_01.nrrd
	set altfilename [$this currentBrainName]
	append altfilename "_01.nrrd"
	set altdir [ file join [file dirname [$this RegRootDir getValue] ] "images4um" ]
	set altpath [ file join $altdir $altfilename ]
	
	# figure out which (if any) of the downsampled images are available
	# echo $filename
	set filewewant ""
	if { [file exists $filename]>0 } {
		set filewewant $filename
	} else {
		if { [file exists $altpath]>0 } {
			set filewewant $altpath
		} else {
		    echo "No image data for current brain at: $filename or $altpath"
		}
	}
	# load the first available resampled image (will prefer lowres.am if available)
	if { [string length filewewant]>0 } {
		# first load voltex
		# or disconnect if it already exists
		$this makeVoltex
		# then load data
		if {[exists curbrain] } {
			remove curbrain
		}
		
		[load $filewewant] setLabel "curbrain"
		# ... and connect and fire
		curbrainview data connect "curbrain"
		
		# set the display range for the voltex viewer
		set mymax [lindex [curbrain getRange ] 1]
		set mymin [expr {$mymax / 20}]
		curbrainview colormap setMinMax  $mymin $mymax
		
		curbrainview doIt hit
		curbrainview fire
	}
}

$this proc setupRegistration { } {
    echo "setup registration"
    $this cleanupRegistration
    
    set refbrain [$this getVar _refbrain]
    # clear transform on refbrain
    $refbrain setTransform 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1

    # make a surface from curbrain
    # first make labels
    create HxLabelVoxel {curbrain.LabelVoxel}
    # LabelVoxel setIconPosition 221 29
    curbrain.LabelVoxel data connect curbrain
    curbrain.LabelVoxel fire
    curbrain.LabelVoxel regions setState {Exterior Inside}
    set mymax [lindex [curbrain getRange ] 1]
	set mythresh [expr {$mymax / 10}]
    curbrain.LabelVoxel boundary01 setValue $mythresh
    curbrain.LabelVoxel fire
    curbrain.LabelVoxel setViewerMask 16383
    curbrain.LabelVoxel setPickable 1
    
    [ {curbrain.LabelVoxel} create
     ] setLabel {curbrain.Labels}
    curbrain.Labels master connect curbrain.LabelVoxel result
    curbrain.Labels ImageData connect curbrain
    curbrain.Labels fire
    
    create HxGMC {curbrain.SurfaceGen}
    curbrain.SurfaceGen setIconPosition 246 73
    curbrain.SurfaceGen data connect curbrain.Labels
    curbrain.SurfaceGen fire

    # set hideNewModules 0
    [ {curbrain.SurfaceGen} create {curbrain.surf}
     ] setLabel {curbrain.surf}
    # curbrain.surf setIconPosition 117 108
    curbrain.surf master connect curbrain.SurfaceGen result
    # curbrain.surf fire
    # curbrain.surf LevelOfDetail setMinMax -1 -1
    # curbrain.surf LevelOfDetail setButtons 1
    # curbrain.surf LevelOfDetail setIncrement 1
    # curbrain.surf LevelOfDetail setValue -1
    # curbrain.surf LevelOfDetail setSubMinMax -1 -1
    curbrain.surf setTransform 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1
    curbrain.surf fire
    curbrain.surf setViewerMask 16383
    curbrain.surf select
    # these would set things up correctly if I could get
    curbrain.surf setEditor
    # to work properly
    # curbrain.surf transformEditorManip setValue Manipulator "TabBox"
    # curbrain.surf transformEditorManip send
    
    create HxDisplaySurface {curbrain.surfview}
    curbrain.surfview data connect curbrain.surf
    curbrain.surfview colormap setDefaultColor 1 0 0
    curbrain.surfview drawStyle setValue 1
    curbrain.surfview drawStyle setAlphaMode 2
    curbrain.surfview fire
    curbrain.surfview baseTrans setValue 0.441558
    curbrain.surfview fire
    
    create HxAlignSurfaces {curbrain.AlignSurfaces}
    curbrain.AlignSurfaces setIconPosition 231 146
    curbrain.AlignSurfaces surface_to_be_transformed connect curbrain.surf
    # connect to reference surface
    curbrain.AlignSurfaces reference_surface connect $refbrain
    curbrain.AlignSurfaces fire
    curbrain.AlignSurfaces select
}
$this proc cleanupRegistration { } {
    echo "clean up registration"
    remove curbrain.Labels curbrain.LabelVoxel curbrain.SurfaceGen curbrain.surf curbrain.surfview curbrain.AlignSurfaces
}

$this proc saveRegistration { } {
    
    if { [curbrain.surf isModified] < 0 } {
        echo "saving registration"
        # nb we need inverse transform to map ref onto sample
        set xform [$this permutexform [curbrain.surf getInverseTransform ] ]
        echo $xform
        set dirname [$this dirForCurrentBrain]
        if { ![file exists $dirname] } {
            file mkdir "$dirname"
        }
        
        set filename [file join $dirname "hxtransform"]
        if { [file exists $filename] && ![file exists "$filename.bak"] } {
            file rename $filename "$filename.bak"
        }
        set fileId [open $filename "w"]
        puts $fileId $xform
        close $fileId
        curbrain.surf setModified 0
        # Now save in CMTK format as well if the relevant check box is ticked
        if {[ResultViewer.hx  SaveRegistrationAsCMTK getValue 0]} {
            $this convertAmiraRegToCMTK $dirname
        }
        
    } else {
        echo "Registration unmodified. Skipping save."
    }
}


$this proc findexecutable { execname } {
	if {[catch {exec which $execname} ]} {
		# not in path, let's look somewhere specific
		set fijibindir "/Applications/Fiji.app/bin/cmtk/"
		set execpath [file join $fijibindir $execname]
		if { [file executable $execpath] } {
			return $execpath
		} else {
			error "Cannot find mat2dof! Cannot write registrations to CMTK format"
		}
	}
	return $execname
}

$this proc convertAmiraRegToCMTK { dirname } {
	# NB mat2dof must be in path
    set cmtkregfile [file join $dirname "registration"]
    set amiraregfile [file join $dirname "hxtransform"]
    
    if { [file exists "$cmtkregfile"] && ![file exists "${cmtkregfile}.bak"] } {
        file rename "$cmtkregfile" "${cmtkregfile}.bak"
    }
	set mat2dof [$this findexecutable mat2dof]
    exec $mat2dof --list "$dirname" < "$amiraregfile"
}
