# Use transform computed by generalized Hough transform to initialize
# registration.
AmiraTransformToIGSRegistration <- function(key, transform.path=config$transform.path) {
    tf <- scan(transform.path(key), what=1.1)
    tf <- matrix(tf, nrow=4)
    igs.params <- DecomposeAffineToIGSParams(tf)
    igs.reg <- IGSParamsToIGSRegistration(igs.params)
    igs.reg
}

# Test with key <- '5HT1bMARCM-F000011_seg001'
# Assuming we have a folder 'Registration', we can fix a registration as
# described at
# http://flybrain.mrc-lmb.cam.ac.uk/dokuwiki/doku.php?id=warping_manual:calculating_an_initial_registration_from_landmarks
OverwriteAffineWithGHT <- function(key,
        transform.path=config$transform.path,
        regdir=config$regdir,
        reg.affine.relpath=config$reg.affine.relpath,
        reg.ght.relpath=config$reg.ght.relpath) {
    abspath <- function(p) {
        file.path(regdir, p)
    }
    oldreg <- ReadIGSRegistration(abspath(reg.affine.relpath(key)), ReturnRegistrationOnly=FALSE)
    newreg <- AmiraTransformToIGSRegistration(key, transform.path=transform.path)
    oldreg$registration$affine_xform <- newreg$registration$affine_xform
    WriteIGSRegistrationFolder(oldreg, abspath(reg.ght.relpath(key)))
    message("Run the following commands on hex to start the affine registration from the ght initialization.")
    message(sprintf("  cd %s", regdir))
    message(sprintf("  /lmb/home/jefferis/dev/neuroanat/cmtk/core/build/bin/registration -v --dofs 6 --dofs 9 --accuracy 0.4 -o %s %s", reg.affine.relpath(key), reg.ght.relpath(key)))
}
