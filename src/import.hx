# Amira Script

if { ![exists Python] } {
    if { [catch {create HxPython}] != 0 } {
        echo "No python!"
        return 1
    }
}

# echo "Still going!"
 
set dir [file dirname [info script]]
Python exec "
dir = '$dir'
"

Python exec {
import sys
if not dir in sys.path:
    sys.path.append(dir)
import ght
reload(ght)
}
