import hx
import os
import sys
import os.path
import re
from subprocess import call
import random

# Call it from a shell like:
#   ~/bin/zibamira -opt -no_gui -logfile /dev/null -tclcmd 'source src/import.hx; Python exec {ght.processDir(root="/Volumes/teraraid/prohaska/projects/TestReg", refbrainsurf="refbrain/FC6475.surf")}'
# regfun should specify a particular function used to do the actual registration
# with a specific set of parameters and perhaps multiple levels
def processDir(root,
        refbrainsurf,
        refkey=None,
        imagedir='images',
        imagegrep='(?P<key>.*)_(?P<channel>01).nrrd$',
        keylistfile=None,
        regdir='Registration',
        regfun='register',
        mat2dofbin='mat2dof'):
    def globImages(root, imagedir, imagegrep, keys):
        regex = re.compile(imagegrep)
        imdir = os.path.join(root, imagedir)
        imgs = os.listdir(imdir)
        # shuffle list as a very stupid way to allow parallel jobs
        random.shuffle(imgs)
        for p in imgs:
            m = regex.match(p)
            if m:
                key = m.group('key')
                if keys and key not in keys:
                    continue
                yield (key, m.group('channel'), os.path.join(imdir, p))
    refkey = refkey or os.path.splitext(os.path.basename(refbrainsurf))[0]
    pool = hx.Pool()
    t = pool.load(os.path.join(root, refbrainsurf))
    store = RegStore(os.path.join(root, regdir))
    store.makedirs()
    keys = None
    if keylistfile:
        keys = loadKeys(keylistfile)
    for key, channel, impath in globImages(root, imagedir, imagegrep, keys):
        if store.has(refkey, key, channel):
            print "Skipping %s." % key
            continue
        print "Processing %s (channel %s)." % (key, channel)
        im = pool.load(impath)
        # figure out which function to call for registration
        # see also http://pyfaq.infogami.com/how-do-i-use-strings-to-call-functions-methods
        if regfun == "register":
            reg = register(template=t, image=im)
        elif regfun == 'registerjlabtofc':
            reg = registerjlabtofc(template=t, image=im)
        elif regfun == 'registerfc':
            reg = registerfc(template=t, image=im)
        elif regfun == 'registerfcsimple':
            reg = registerfcsimple(template=t, image=im)
        elif regfun == 'registerfcxlateonly':
            reg = registerfcxlateonly(template=t, image=im)
        else:
            sys.exit("No registration function %s." % regfun)
        store.saveTransform(refkey, key, channel, reg['transformed'])
        store.convertTransform(refkey, key, channel, mat2dofbin)
        store.saveLowres(refkey, key, channel, reg['filtered'])

def loadKeys(path):
    return open(path).read().rstrip().split("\n")

class RegStore(object):
    def __init__(self, regdir, pool=None):
        self.ghtdir = os.path.join(regdir, 'affine')
        self.pool = pool

    def _outdir(self, refkey, key, channel):
        return os.path.join(self.ghtdir, "%s_%s_%s_ght.list" % (refkey, key, channel))

    def _hxtransformpath(self, refkey, key, channel):
        return os.path.join(self._outdir(refkey, key, channel), "hxtransform")

    def _hxlowrespath(self, refkey, key, channel):
        return os.path.join(self._outdir(refkey, key, channel), "lowres.am")

    def makedirs(self):
        if not os.path.exists(self.ghtdir):
            os.makedirs(self.ghtdir)

    def has(self, refkey, key, channel):
        d = self._outdir(refkey, key, channel)
        return os.path.isdir(d) and \
                os.path.isfile(os.path.join(d, "registration")) and \
                os.path.isfile(os.path.join(d, "studylist")) and \
                os.path.isfile(self._hxtransformpath(refkey, key, channel))

    @staticmethod
    def _transpose(t):
        return [t[i*4 + j] for j in range(4) for i in range(4)]

    def saveTransform(self, refkey, key, channel, obj):
        def _getTransform(obj):
            return obj._tcl(["getTransform"])
        def _save(refkey, key, channel, t):
            d = self._outdir(refkey, key, channel)
            if not os.path.exists(d):
                os.mkdir(d)
            fp = open(self._hxtransformpath(refkey, key, channel), 'w')
            fp.write(t)
            fp.write('\n')
            fp.close()
        t = _getTransform(obj)
        t = " ".join(self._transpose(t.split()))
        _save(refkey, key, channel, t)

    def loadTransform(self, refkey, key, channel, obj):
        tf = open(self._hxtransformpath(refkey, key, channel)).read().split()
        tf = self._transpose(tf)
        obj._tcl(["setTransform"] + tf)

    def convertTransform(self, refkey, key, channel, mat2dofbin):
        d = self._outdir(refkey, key, channel)
        try:
            call('%s --list . <hxtransform' % mat2dofbin, cwd=d, shell=True)
        except OSError:
            # Spurious errors seem to occur when using call from inside amira.
            # So ignore errors...
            pass
            # ... and instead check if the output files have been created.
        if not os.path.isfile(os.path.join(d, "registration")) or \
                not os.path.isfile(os.path.join(d, "studylist")):
            print "Warning: mat2dof has failed to process '%s'." % d

    def saveLowres(self, refkey, key, channel, img):
        img.save(self._hxlowrespath(refkey, key, channel))

    def loadLowres(self, refkey, key, channel):
        return self.pool.load(self._hxlowrespath(refkey, key, channel))

# Use the following commands to inspect several results:
# cd /path/to/workdir/
# source /path/to/ght-dir/src/import.hx
# Python exec {keys = ght.loadKeys('keylist.txt')}
# for {set i 0} {$i < 10} {incr i} { echo $i; Python exec "view = ght.show(keys\[$i\], refbrainsurf='refbrain/FC6475.surf')" ; viewer 0 redraw; sleep 1 }
# See also ResultViewer.hx which automates a lot of this
def show(key,
        refbrainsurf,
        root='.',
        refkey=None,
        channel='01',
        regdir='Registration'):
    refkey = refkey or os.path.splitext(os.path.basename(refbrainsurf))[0]
    pool = hx.Pool()
    store = RegStore(os.path.join(root, regdir), pool)
    img = store.loadLowres(refkey, key, channel)
    template = pool.load(os.path.join(root, refbrainsurf))
    store.loadTransform(refkey, key, channel, template)
    def _volren(img):
        vr = pool.create("HxVoltex")
        vr.port.data.connect(img)
        vr.port.texture2D3D.setValue(1)
        vr.port.slices.setValue(512)
        vr.port.alphaScale.setValue(0.5)
        vr.port.doIt.hit()
        vr.fire()
        vr.fire()
        return vr
    vr = _volren(img)
    def _surfview(surf):
        sv = pool.create("HxDisplaySurface")
        sv.port.data.connect(surf)
        LINES = 2
        sv.port.drawStyle.setValue(LINES)
        sv.fire()
        return sv
    sv = _surfview(template)
    return { 'image':img, 'volren':vr, 'template':template, 'surfview':sv }


# Simpler, lowres approach; see [spr note 2011-09-14]
def register(template, image):
    image = resample(image, voxelSize=[8, 8, 8])
    image = smoothGauss(image, sigma=1, width=5)
    grad = gradient(image)
    mag = magnitude(grad)
    mask = thresholdFloat(mag, threshold=20)  # threshold depends on image content.
    # Search full z rotation range for initialization.
    tf = ghough(template, grad, mask, invertNormals=True, zrot={'range':360, 'steps':60})
    # Search scale for restricted zrot (fast and might be good enough).
    tf = ghough(tf, grad, mask, invertNormals=True,
            scale={'range':[0.8, 1.2], 'steps':9},
            zrot={'range':30, 'steps':6}
            )
    return { 'transformed':tf,
             'filtered':image,
             'gradient':grad,
             'mask':mask }

# Flycircuit brains do not need much rotation because they have been imaged
# almost wihthout exception in a vertical orientation
def registerfc(template, image):
    image = resample(image, voxelSize=[8, 8, 8])
    image = smoothGauss(image, sigma=1, width=5)
    grad = gradient(image)
    mag = magnitude(grad)
    mask = thresholdFloat(mag, threshold=20)  # threshold depends on image content.
    # Search full z rotation range for initialization.
    tf = ghough(template, grad, mask, invertNormals=True, zrot={'range':60, 'steps':10})
    # Search scale for restricted zrot (fast and might be good enough).
    tf = ghough(tf, grad, mask, invertNormals=True,
             scale={'range':[0.8, 1.2], 'steps':7},
             zscale={'range':[0.8, 1.2], 'steps':7},
             zrot={'range':30, 'steps':5}
             )
    return { 'transformed':tf,
          'filtered':image,
          'gradient':grad,
          'mask':mask }

# Flycircuit brains do not need much rotation because they have been imaged
# almost wihthout exception in a vertical orientation.
# this simple version just cuts the initial amount of Z rotation but is 
# otherwise identical to Steffen's initial version
def registerfcsimple(template, image):
    image = resample(image, voxelSize=[8, 8, 8])
    image = smoothGauss(image, sigma=1, width=5)
    grad = gradient(image)
    mag = magnitude(grad)
    magrange = mag._tcl(["getRange"])
    magmax = float(magrange.split(' ')[1])
    # originally had this at 20 for 12 bit data where max of 189 was observed
    # a sample 8 bit image had range 13.9
    mask = thresholdFloat(mag, threshold=magmax/10)  # threshold depends on image content.
    # Search restricted z rotation range for initialization.
    tf = ghough(template, grad, mask, invertNormals=True, zrot={'range':60, 'steps':10})
    # Search scale for restricted zrot (fast and might be good enough).
    tf = ghough(tf, grad, mask, invertNormals=True,
            scale={'range':[0.8, 1.2], 'steps':9},
            zrot={'range':30, 'steps':6}
            )
    return { 'transformed':tf,
             'filtered':image,
             'gradient':grad,
             'mask':mask }

def registerfcxlateonly(template, image):
    image = resample(image, voxelSize=[8, 8, 8])
    image = smoothGauss(image, sigma=1, width=5)
    grad = gradient(image)
    mag = magnitude(grad)
    magrange = mag._tcl(["getRange"])
    magmax = float(magrange.split(' ')[1])
    # originally had this at 20 for 12 bit data where max of 189 was observed
    # a sample 8 bit image had range 13.9
    mask = thresholdFloat(mag, threshold=magmax/10)  # threshold depends on image content.
    # Search restricted z rotation range for initialization.
    tf = ghough(template, grad, mask, invertNormals=True)
    return { 'transformed':tf,
             'filtered':image,
             'gradient':grad,
             'mask':mask }

# test function used by Greg and Philip 2011-12-20
# to register some jlab brains onto FC6475 flycircuit template
# rename to register to use in place of original
def registerjlabtofc(template, image):
     image = resample(image, voxelSize=[8, 8, 8])
     image = smoothGauss(image, sigma=1, width=5)
     grad = gradient(image)
     mag = magnitude(grad)
     mask = thresholdFloat(mag, threshold=20)  # threshold depends on image content.
     tf = ghough(template, grad, mask, invertNormals=True, zrot={'range':360, 'steps':30}, zscale={'range':[.7, 1.4], 'steps':2})
     # Search scale for restricted zrot (fast and might be good enough).
     tf = ghough(tf, grad, mask, invertNormals=True,
             zscale={'range':[.9, 1.1], 'steps':3},
             scale={'range':[.85, 1.15], 'steps':5},
             zrot={'range':30, 'steps':6}
             )
     return { 'transformed':tf,
              'filtered':image,
              'gradient':grad,
              'mask':mask }

def resample(image, voxelSize):
    MODE_VOXEL_SIZE = 1
    res = hx.create('HxResample')
    res.port.data.connect(image)
    res.fire()
    res.port.mode.setValue(MODE_VOXEL_SIZE)
    for d in range(3):
        res.port.voxelSize.setValue(d, voxelSize[d])
    res.port.action.hit()
    res.fire()
    ret = res.result[0]
    ret.port.master.disconnect()
    return ret

def smoothGauss(image, sigma, width):
    f = hx.create("HxImageFilters")
    f.port.data.connect(image)
    f.fire()
    GAUSS = 5
    f.port.filter.setValue(GAUSS)
    f.fire()
    XY = 2
    f.port.filter.setValue(1, XY)
    f.fire()
    for i in range(2):
        f.port.sigma.setValue(i, sigma)
        f.port.kernel.setValue(i, width)
    f.port.doIt.hit()
    f.fire()
    res = f.result[0]
    return res

def gradient(image):
    grad = hx.create('HxGradient')
    grad.port.data.connect(image)
    grad.fire()
    FLOAT = 1
    grad.port.resulttype.setValue(FLOAT)
    grad.port.doIt.hit()
    grad.fire()
    ret = grad.result[0]
    ret.port.master.disconnect()
    return ret

def magnitude(grad):
    m = hx.create('HxMagnitude')
    m.port.data.connect(grad)
    m.fire()
    res = m.result[0]
    res.port.master.disconnect()
    return res

def thresholdFloat(mag, threshold):
    def _threshold(mag, threshold):
        arith = hx.create('HxArithmetic')
        arith.port.inputA.connect(mag)
        arith.fire()
        arith.port.expr0.setValue('a > %f' % threshold)
        arith.port.doIt.hit()
        arith.fire()
        return arith.result[0]
    def _cast(f):
        c = hx.create('HxCastField')
        c.port.data.connect(f)
        c.fire()
        LABELS = 6
        c.port.outputType.setValue(LABELS)
        c.fire()
        POSTPROCESSING = 0
        c.port.voxelGridOptions.setValue(POSTPROCESSING, 0)
        c.port.action.hit()
        c.fire()
        return c.result[0]
    binary = _threshold(mag, threshold)
    labels = _cast(binary)
    labels.port.master.disconnect()
    return labels

def ghough(template, gradient, mask, **kwargs):
    dup = template.duplicate()
    ght = hx.create('HxGeneralizedHoughTransform')
    ght.port.TemplateShape.connect(dup)
    ght.port.ImageGradients.connect(gradient)
    ght.port.Mask.connect(mask)
    ght.fire()
    def _parseScaleArg(port, name):
        if name in kwargs:
            _activatePort(port)
            _setScaleRange(port, kwargs[name])
    def _parseRotArg(port, name):
        if name in kwargs:
            _activatePort(port)
            _setRotRange(port, kwargs[name])
    def _activatePort(port):
        port.setValue(0, 1)
    def _setScaleRange(port, config):
        port.setValue(2, config['range'][0])
        port.setValue(4, config['range'][1])
        port.setValue(6, config['steps'])
    def _setRotRange(port, config):
        port.setValue(2, config['range'])
        port.setValue(4, config['steps'])
    _parseScaleArg(ght.port.Scale, 'scale')
    _parseScaleArg(ght.port.ZScale, 'zscale')
    _parseRotArg(ght.port.XRotation, 'xrot')
    _parseRotArg(ght.port.YRotation, 'yrot')
    _parseRotArg(ght.port.ZRotation, 'zrot')
    if 'invertNormals' in kwargs and kwargs['invertNormals']:
        ght.port.InvertNormals.setValue(0, 1)
    ght.port.Threads.setValue(1)
    ght.port.action.hit()
    ght.fire()
    ght.port.TemplateShape.disconnect()
    # debug
    # hx.Pool()['ght'] = ght
    # END debug
    return dup


def test_register(key=None, imagedir='data/images'):
    key = key or 'ChaMARCM-F001348_seg001'
    pool = hx.Pool()
    t = pool.load('hx/FC6475-flip_01-filtered-2011-09-14.surf')
#    im = pool.load('data/FruMARCM-M002116_seg001_r4_01.nrrd')
    im = pool.load('%s/%s_01.nrrd' % (imagedir, key))
    reg = register(t, im)
    pool['transformed'] = reg['transformed']
    pool['filtered'] = reg['filtered']
    pool['gradient'] = reg['gradient']
    pool['mask'] = reg['mask']
    reg['template'] = t
    pool['template'] = t
    reg['image'] = im
    pool['image'] = im
    return reg

def test_specialCases(key=None):
    key = key or 'DvGlutMARCM-F002273_seg001'
    pool = hx.Pool()
    r = test_register(key, imagedir='/Volumes/teraraid/jefferis/projects/ChiangReanalysis/ChiangReg/images')
    if key == 'ChaMARCM-F001348_seg001':
        tf = ghough(r['template'], r['gradient'], r['mask'],
                invertNormals=True,
                zrot={'range':360, 'steps':60},
                scale={'range':[0.8, 1.2], 'steps':5})
        tf = ghough(tf, r['gradient'], r['mask'],
                invertNormals=True,
                scale={'range':[0.8, 1.2], 'steps':9},
                zrot={'range':30, 'steps':6}
                )
    elif key == 'ChaMARCM-F001352_seg001':
        tf = ghough(r['template'], r['gradient'], r['mask'],
                invertNormals=True,
                scale={'range':[1, 1.2], 'steps':5})
    elif key == 'ChaMARCM-F001356_seg001':
        tf = r['template'] # It works with fixed zrot steps.
    elif key == 'ChaMARCM-F001358_seg001':
        tf = ghough(r['template'], r['gradient'], r['mask'],
                invertNormals=True,
                zrot={'range':360, 'steps':60})
    elif key == 'ChaMARCM-F001373_seg001':
        tf = ghough(r['template'], r['gradient'], r['mask'],
                invertNormals=True,
                zrot={'range':360, 'steps':60})
    elif key == 'DvGlutMARCM-F002273_seg001':
        tf = ghough(r['template'], r['gradient'], r['mask'],
                invertNormals=True,
                scale={'range':[0.8, 1.2], 'steps':9})
    pool['transformed-fixed'] = tf
    r['transformed-fixed'] = tf
    return r
