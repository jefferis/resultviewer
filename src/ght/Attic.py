# Call it like
# From console,
#   Python exec {ght.processImages(idxRange=(0, 10))}
# From shell,
#   zibamira -opt -no_gui -logfile /dev/null -tclcmd 'source src/import.hx; Python exec {ght.processImages(idxRange=(5000, 8000))}'
# To process all images, simply run,
#   zibamira -opt -no_gui -logfile /dev/null -tclcmd 'source src/import.hx; Python exec {ght.processImages()}'
def processImages(idxRange=None):
    keys = loadKeys()
    if idxRange:
        idxRange = range(idxRange[0], idxRange[1])
    else:
        idxRange = range(0, len(keys))
    pool = hx.Pool()
#    t = pool.load('hx/FBFemale1-dilate40u-maxDist8u.surf')
    t = pool.load('hx/FC6475-flip_01-filtered-2011-09-14.surf')
    imgstore = ImageStore(pool)
    tfstore = TransformStore()
    for i in idxRange:
        k = keys[i]
        if imgstore.hasLowres(k) and tfstore.has(k):
            print "Keeping previous result for image #%d '%s'." % (i, k)
            continue
        print "Processing image #%d '%s' ..." % (i, k)
        im = imgstore.load(k)
        reg = register(template=t, image=im)
        tfstore.save(k, reg['transformed'])
        lowres = reg['filtered']
        imgstore.saveLowres(k, lowres)
    print "Done."

def loadKeys():
    return open('data/image.names.txt').read().rstrip().split("\n")

# Use the following commands to inspect several results:
# Python exec {keys = ght.loadKeys()}
# for {set i 0} {$i < 10} {incr i} { echo $i; Python exec "view = ght.show(keys\[$i\])" ; viewer 0 redraw; sleep 1 }
# for {set i 0} {$i < 10} {incr i} { echo $i; Python exec "view = ght.show(keys\[$i\])" ; viewer 0 redraw; sleep 1 }
def show(key):
    pool = hx.Pool()
    imgstore = ImageStore(pool)
    tfstore = TransformStore()
    img = imgstore.loadLowres(key)
    def _volren(img):
        vr = pool.create("HxVoltex")
        vr.port.data.connect(img)
        vr.port.texture2D3D.setValue(1)
        vr.port.slices.setValue(512)
        vr.port.alphaScale.setValue(0.5)
        vr.port.doIt.hit()
        vr.fire()
        # For unknown reasons, a freshly created Voltex is hidden after
        # hideAll() has been called, so we explicitly show it.
        vr._tcl(['setViewerMask', 16383])
        vr.fire()
        return vr
    vr = _volren(img)
    template = pool.load('hx/FC6475-flip_01-filtered-2011-09-14.surf')
    tfstore.load(key, template)
    def _surfview(surf):
        sv = pool.create("HxDisplaySurface")
        sv.port.data.connect(surf)
        LINES = 2
        sv.port.drawStyle.setValue(LINES)
        sv.fire()
        return sv
    sv = _surfview(template)
    return { 'image':img, 'volren':vr, 'template':template, 'surfview':sv }

def hideAll():
    pool = hx.Pool(managed=False)
    for k in pool.keys():
        try:
            pool[k]._tcl(["setViewerMask", 0])
        except Exception:
            pass # ignore

class ImageStore(object):
    def __init__(self, pool):
        self.pool = pool

    def load(self, key):
        return self.pool.load('/Volumes/teraraid/jefferis/projects/ChiangReanalysis/ChiangReg/images/%s_01.nrrd' % key)

    def saveLowres(self, key, img):
        img.save(self._lowresPath(key))

    def loadLowres(self, key):
        return self.pool.load(self._lowresPath(key))

    def hasLowres(self, key):
        return os.path.isfile(self._lowresPath(key))

    def _lowresPath(self, key):
        return 'data/lowres/%s.am' % key

class TransformStore(object):
    def save(self, key, obj):
        t = obj._tcl(["getTransform"])
        fp = open(self._path(key), 'w')
        fp.write(t)
        fp.write('\n')
        fp.close()

    def load(self, key, obj):
        tf = open(self._path(key)).read().split(" ")
        obj._tcl(["setTransform"] + tf)

    def has(self, key):
        return os.path.isfile(self._path(key))

    def _path(self, key):
        return 'data/transforms/%s.txt' % key

# First try.
def test():
    pool = hx.Pool(managed=False)
    t = pool['FBFemale1-dilate40u-maxDist8u.surf']
    im = pool['FruMARCM-M002116_seg001_r4_01.nrrd']
    tf = ght(template=t, image=im)
    pool['transformed'] = tf['template']
    pool['mask'] = tf['mask']
    pool['labels'] = tf['labels']
    pool['gradient'] = tf['gradient']

def ght(template, image):
    image = resample(image, voxelSize=[4, 4, 4])
    labels = threshold(image, threshold=400)
    labels = pad(labels)
    dt = dist(labels)
    (grad, mask) = gradientMaskFromDist(dt, threshold=40, sigma=5)
    def _ght(template, gradient, mask):
        dup = template.duplicate()
        ght = hx.create('HxGeneralizedHoughTransform')
        ght.port.TemplateShape.connect(dup)
        ght.port.ImageGradients.connect(gradient)
        ght.port.Mask.connect(mask)
        ght.fire()
        ght.port.Scale.setValue(0, 1)
        ght.port.ZScale.setValue(0, 1)
        ght.port.XRotation.setValue(0, 1)
        ght.port.YRotation.setValue(0, 1)
        ght.port.ZRotation.setValue(0, 1)
        MANY = 100000
        ght.port.Threads.setValue(MANY)
        ght.port.action.hit()
        ght.fire()
        ght.port.TemplateShape.disconnect()
        return dup
    return { 'template':_ght(template, grad, mask),
             'labels':labels,
             'gradient':grad,
             'mask':mask }

def threshold(image, threshold):
    lv = hx.create('HxLabelVoxel')
    lv.port.data.connect(image)
    lv.port.regions.setValue("Exterior Interior")
    lv.fire()
    lv.port.boundary01.setValue(threshold)
    lv.port.action.hit()
    lv.fire()
    ret = lv.result[0]
    ret.port.master.disconnect()
    return ret

def pad(image):
    image = image.duplicate()
    dims = map(int, image._tcl(["getDims"]).split())
    image._tcl(["crop", -100, dims[0] + 100, -100, dims[1] + 100, -100, dims[2] + 100, 0])
    return image

def dist(labels):
    dt = hx.create('HxDistanceTransform')
    dt.port.data.connect(labels)
    dt.fire()
    dt.port.doIt.hit()
    dt.fire()
    ret = dt.result[0]
    ret.port.master.disconnect()
    return ret

def gradientMaskFromDist(dist, threshold, sigma):
    def _surf():
        arith = hx.create('HxArithmetic')
        arith.port.inputA.connect(dist)
        arith.fire()
        arith.port.expr0.setValue('1 / (1 + exp(-(a - %f) / %f))' % (threshold, sigma))
        arith.port.doIt.hit()
        arith.fire()
        return arith.result[0]
    def _grad(surf):
        grad = hx.create('HxGradient')
        grad.port.data.connect(surf)
        grad.port.doIt.hit()
        grad.fire()
        ret = grad.result[0]
        ret.port.master.disconnect()
        return ret
    def _mask(surf):
        arith = hx.create('HxArithmetic')
        arith.port.inputA.connect(surf)
        arith.fire()
        arith.port.expr0.setValue('a > 0.4 && a < 0.6')
        arith.port.doIt.hit()
        arith.fire()
        mask = arith.result[0]
        cast = hx.create('HxCastField')
        cast.port.data.connect(mask)
        cast.fire()
        OD_LABEL_FIELD = 6
        cast.port.outputType.setValue(OD_LABEL_FIELD)
        cast.port.action.hit()
        cast.fire()
        ret = cast.result[0]
        ret.port.master.disconnect()
        return ret
    surf = _surf()
    return (_grad(surf), _mask(surf))

# Note 2011-09-26:
# Visual inspection of keys[2000:2500] revealed a few cases in which ght failed:
# Python exec {print [keys[i] for i in (2003, 2004, 2008, 2009, 2013, 2015, 2042, 2125)]}
# ['ChaMARCM-F001348_seg001', 'ChaMARCM-F001348_seg002', 'ChaMARCM-F001352_seg001', 'ChaMARCM-F001352_seg002', 'ChaMARCM-F001356_seg001', 'ChaMARCM-F001358_seg001', 'ChaMARCM-F001373_seg001', 'DvGlutMARCM-F002273_seg001']

# Note 2011-09-29:
# Examples that failed without initialization:
# in /Volumes/JData/JPeople/Bertalan/FlyCircuit/RegistrationQuality/dodgybad
# 'ChaMARCM-F000445_seg001' 'ChaMARCM-F000775_seg001' 'DvGlutMARCM-F025_seg1' 'DvGlutMARCM-F392_seg1' 'DvGlutMARCM-F1990_seg1' 'GadMARCM-F000423_seg001'
