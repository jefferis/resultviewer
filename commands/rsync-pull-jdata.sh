#!/bin/bash

set -o errexit
set -o nounset

[ "$(basename $(pwd))" = "2011-07-29_InitBrainPosGHT" ] || { echo "Please run script in project directory."; exit 1; }

: ${HOST:=lmbmac}
echo "Pulling from '$HOST' (use HOST to override)."
rsync -rav --update $HOST:/Volumes/JData/JPeople/Steffen/2011-07-29_InitBrainPosGHT/data/lowres/ ./data/lowres/
rsync -rav --update $HOST:/Volumes/JData/JPeople/Steffen/2011-07-29_InitBrainPosGHT/data/transforms/ ./data/transforms/
