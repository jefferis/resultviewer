# ResultViewer and Generalised Hough Transform

## GHT
Module to carry out robust coarse initial registration of brains (solving pose problem) using Generalised Hough Transform written in python for use with ZIB Amira. Contact http://www.zib.de/members/prohaska for details.

## ResultViewer.hx

A script object that can be dragged onto Amira. It allows a folder containing affine CMTK registrations to be applied to image data and then compared with a reference brain to see if the registration was satisfactory. If this is not the case, a new registration can
be calculated either using the Generalised Hough Transform (separate plugin) or a surface based registration.

## StackViewer.hx

This lets you review a directory of Amira-compatible images (nrrd, am etc). You can filter images to view by a shell style glob and select images as you go along.

To get started:

0. Download or clone the repository
1. Drag and Drop StackViewer.hx onto Amira
2. Choose a Stack Directory (ignore the keylist field)
3. Start scanning through the images
4. Choose your reference brain or (hide it)
5. Tick the selection box to make a note of images you like
6. Save Selection periodically in case Amira crashes
7. If you restart, you need to load your selection
