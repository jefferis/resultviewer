# Amira Script
remove -all
remove grey.am redblue.icol FruMARCM-M002116_seg001_r4_01.nrrd FruMARCM-M002116_seg001_r4_01.nrrd.Gradient FruMARCM-M002116_seg001_r4_01.nrrd.Resampled FruMARCM-M002116_seg001_r4_01.Resampled FruMARCM-M002116_seg001_r4_01.Labels FruMARCM-M002116_seg001_r4_01.DistField Result FC6475_r4-filtered.surf Result.Labelfield FruMARCM-M002116_seg001_r4_01.nrrd-accumulator FC6475_r4-filtered2.surf Gradient Resample Resample2 LabelVoxel OrthoSlice DistanceMap Arithmetic Isosurface GeneralizedHoughTransform3D CastField SurfaceView OrthoSlice2 OrthoSlice3 EmptyPlane Vectors Scale

# Create viewers
viewer setVertical 0

viewer 0 setBackgroundMode 1
viewer 0 setBackgroundColor 0.0980392 0.298039 0.298039
viewer 0 setBackgroundColor2 0.686275 0.701961 0.807843
viewer 0 setTransparencyType 5
viewer 0 setAutoRedraw 0
viewer 0 show
mainWindow show

set hideNewModules 1
[ load ${AMIRA_ROOT}/data/colormaps/grey.am ] setLabel grey.am
grey.am setIconPosition 0 0
grey.am setNoRemoveAll 1
grey.am fire
{grey.am} setMinMax 0 255
grey.am flags setValue 1
grey.am shift setMinMax -1 1
grey.am shift setButtons 0
grey.am shift setIncrement 0.133333
grey.am shift setValue 0
grey.am shift setSubMinMax -1 1
grey.am scale setMinMax 0 1
grey.am scale setButtons 0
grey.am scale setIncrement 0.1
grey.am scale setValue 1
grey.am scale setSubMinMax 0 1
grey.am fire
grey.am setViewerMask 16383

set hideNewModules 1
[ load ${AMIRA_ROOT}/data/colormaps/redblue.icol ] setLabel redblue.icol
redblue.icol setIconPosition 0 0
redblue.icol fire
{redblue.icol} setMinMax 0 0.5
redblue.icol flags setValue 1
redblue.icol shift setMinMax -1 1
redblue.icol shift setButtons 0
redblue.icol shift setIncrement 0.133333
redblue.icol shift setValue 0
redblue.icol shift setSubMinMax -1 1
redblue.icol scale setMinMax 0 1
redblue.icol scale setButtons 0
redblue.icol scale setIncrement 0.1
redblue.icol scale setValue 1
redblue.icol scale setSubMinMax 0 1
redblue.icol fire
redblue.icol setViewerMask 16383

set hideNewModules 0
[ load /Volumes/teraraid/prohaska/projects/JaneliaBrainAligner/Registration/warp/FruMARCM-M002116_seg001/tmp/FruMARCM-M002116_seg001_r4_01.nrrd ] setLabel FruMARCM-M002116_seg001_r4_01.nrrd
FruMARCM-M002116_seg001_r4_01.nrrd setIconPosition 20 257
FruMARCM-M002116_seg001_r4_01.nrrd fire
FruMARCM-M002116_seg001_r4_01.nrrd setViewerMask 16383

set hideNewModules 0
[ load ${SCRIPTDIR}/FruMARCM-M002116_seg001-files/FC6475_r4-filtered.surf ] setLabel FC6475_r4-filtered.surf
FC6475_r4-filtered.surf setIconPosition 25 713
FC6475_r4-filtered.surf fire
FC6475_r4-filtered.surf LevelOfDetail setMinMax -1 -1
FC6475_r4-filtered.surf LevelOfDetail setButtons 1
FC6475_r4-filtered.surf LevelOfDetail setIncrement 1
FC6475_r4-filtered.surf LevelOfDetail setValue -1
FC6475_r4-filtered.surf LevelOfDetail setSubMinMax -1 -1
FC6475_r4-filtered.surf setTransform 1.09874 -0.00867303 0.051825 0 0.00632103 1.09885 0.049953 0 -0.052161 -0.0495949 1.09764 0 97.5997 -39.8853 -12.1089 1
FC6475_r4-filtered.surf fire
FC6475_r4-filtered.surf setViewerMask 16383

set hideNewModules 0
[ load ${SCRIPTDIR}/FruMARCM-M002116_seg001-files/FC6475_r4-filtered2.surf ] setLabel FC6475_r4-filtered2.surf
FC6475_r4-filtered2.surf setIconPosition 19 746
FC6475_r4-filtered2.surf fire
FC6475_r4-filtered2.surf LevelOfDetail setMinMax -1 -1
FC6475_r4-filtered2.surf LevelOfDetail setButtons 1
FC6475_r4-filtered2.surf LevelOfDetail setIncrement 1
FC6475_r4-filtered2.surf LevelOfDetail setValue -1
FC6475_r4-filtered2.surf LevelOfDetail setSubMinMax -1 -1
FC6475_r4-filtered2.surf setTransform 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1
FC6475_r4-filtered2.surf fire
FC6475_r4-filtered2.surf setViewerMask 16383

set hideNewModules 0
create HxGradient {Gradient}
Gradient setIconPosition 160 287
Gradient data connect FruMARCM-M002116_seg001_r4_01.nrrd
Gradient fire
Gradient resulttype setValue 1
Gradient mode setIndex 0 1
Gradient fire
Gradient setViewerMask 16383
Gradient setPickable 1

set hideNewModules 0
[ {Gradient} create result ] setLabel {FruMARCM-M002116_seg001_r4_01.nrrd.Gradient}
FruMARCM-M002116_seg001_r4_01.nrrd.Gradient setIconPosition 20 317
FruMARCM-M002116_seg001_r4_01.nrrd.Gradient master connect Gradient result
FruMARCM-M002116_seg001_r4_01.nrrd.Gradient fire
FruMARCM-M002116_seg001_r4_01.nrrd.Gradient setViewerMask 16383

set hideNewModules 0
create HxResample {Resample}
Resample setIconPosition 160 347
Resample data connect FruMARCM-M002116_seg001_r4_01.nrrd.Gradient
{Resample} fire
Resample filter setIndex 0 4
Resample mode setValue 0
Resample resolution setMinMax 0 1 2147483648
Resample resolution setValue 0 60
Resample resolution setMinMax 1 1 2147483648
Resample resolution setValue 1 60
Resample resolution setMinMax 2 1 2147483648
Resample resolution setValue 2 60
Resample voxelSize setMinMax 0 -3.40282346638529e+38 3.40282346638529e+38
Resample voxelSize setValue 0 4.97418260574341
Resample voxelSize setMinMax 1 -3.40282346638529e+38 3.40282346638529e+38
Resample voxelSize setValue 1 4.97418260574341
Resample voxelSize setMinMax 2 -3.40282346638529e+38 3.40282346638529e+38
Resample voxelSize setValue 2 2.03333330154419
Resample average setMinMax 0 1 32
Resample average setValue 0 2
Resample average setMinMax 1 1 32
Resample average setValue 1 2
Resample average setMinMax 2 1 32
Resample average setValue 2 1
Resample fire
Resample setViewerMask 16383
Resample setPickable 1

set hideNewModules 0
[ {Resample} create
 ] setLabel {FruMARCM-M002116_seg001_r4_01.nrrd.Resampled}
FruMARCM-M002116_seg001_r4_01.nrrd.Resampled setIconPosition 20 377
FruMARCM-M002116_seg001_r4_01.nrrd.Resampled master connect Resample result
FruMARCM-M002116_seg001_r4_01.nrrd.Resampled fire
FruMARCM-M002116_seg001_r4_01.nrrd.Resampled setViewerMask 16383

set hideNewModules 0
create HxResample {Resample2}
Resample2 setIconPosition 160 407
Resample2 data connect FruMARCM-M002116_seg001_r4_01.nrrd
{Resample2} fire
Resample2 filter setIndex 0 4
Resample2 mode setValue 0
Resample2 resolution setMinMax 0 1 2147483648
Resample2 resolution setValue 0 64
Resample2 resolution setMinMax 1 1 2147483648
Resample2 resolution setValue 1 64
Resample2 resolution setMinMax 2 1 2147483648
Resample2 resolution setValue 2 64
Resample2 voxelSize setMinMax 0 -3.40282346638529e+38 3.40282346638529e+38
Resample2 voxelSize setValue 0 4.66329622268677
Resample2 voxelSize setMinMax 1 -3.40282346638529e+38 3.40282346638529e+38
Resample2 voxelSize setValue 1 4.66329622268677
Resample2 voxelSize setMinMax 2 -3.40282346638529e+38 3.40282346638529e+38
Resample2 voxelSize setValue 2 1.90625
Resample2 average setMinMax 0 1 32
Resample2 average setValue 0 2
Resample2 average setMinMax 1 1 32
Resample2 average setValue 1 2
Resample2 average setMinMax 2 1 32
Resample2 average setValue 2 1
Resample2 fire
Resample2 setViewerMask 16383
Resample2 setPickable 1

set hideNewModules 0
[ {Resample2} create
 ] setLabel {FruMARCM-M002116_seg001_r4_01.Resampled}
FruMARCM-M002116_seg001_r4_01.Resampled setIconPosition 20 437
FruMARCM-M002116_seg001_r4_01.Resampled master connect Resample2 result
FruMARCM-M002116_seg001_r4_01.Resampled fire
FruMARCM-M002116_seg001_r4_01.Resampled setViewerMask 16383

set hideNewModules 0
create HxLabelVoxel {LabelVoxel}
LabelVoxel setIconPosition 160 467
LabelVoxel data connect FruMARCM-M002116_seg001_r4_01.Resampled
LabelVoxel fire
LabelVoxel regions setState {Exterior Bone}
LabelVoxel boundary01 setMinMax 0 4681
LabelVoxel boundary01 setButtons 1
LabelVoxel boundary01 setIncrement 10
LabelVoxel boundary01 setValue 213
LabelVoxel boundary01 setSubMinMax 0 4681
LabelVoxel boundary12 setMinMax 0 4681
LabelVoxel boundary12 setButtons 1
LabelVoxel boundary12 setIncrement 10
LabelVoxel boundary12 setValue 970
LabelVoxel boundary12 setSubMinMax 0 4681
LabelVoxel boundary23 setMinMax 0 4681
LabelVoxel boundary23 setButtons 1
LabelVoxel boundary23 setIncrement 10
LabelVoxel boundary23 setValue 1100
LabelVoxel boundary23 setSubMinMax 0 4681
LabelVoxel boundary34 setMinMax 0 4681
LabelVoxel boundary34 setButtons 1
LabelVoxel boundary34 setIncrement 10
LabelVoxel boundary34 setValue 2000
LabelVoxel boundary34 setSubMinMax 0 4681
LabelVoxel options setValue 0 0
LabelVoxel options setToggleVisible 0 1
LabelVoxel options setValue 1 0
LabelVoxel options setToggleVisible 1 1
LabelVoxel options setValue 2 1
LabelVoxel options setToggleVisible 2 1
LabelVoxel action snap 0 1
LabelVoxel fire
LabelVoxel setViewerMask 16383
LabelVoxel setPickable 1

set hideNewModules 0
[ {LabelVoxel} create
 ] setLabel {FruMARCM-M002116_seg001_r4_01.Labels}
FruMARCM-M002116_seg001_r4_01.Labels setIconPosition 20 457
FruMARCM-M002116_seg001_r4_01.Labels master connect LabelVoxel result
FruMARCM-M002116_seg001_r4_01.Labels ImageData connect FruMARCM-M002116_seg001_r4_01.Resampled
FruMARCM-M002116_seg001_r4_01.Labels fire
FruMARCM-M002116_seg001_r4_01.Labels primary setIndex 0 0
FruMARCM-M002116_seg001_r4_01.Labels fire
FruMARCM-M002116_seg001_r4_01.Labels setViewerMask 16383

set hideNewModules 0
create HxComputeDistanceMap {DistanceMap}
DistanceMap setIconPosition 160 497
DistanceMap data connect FruMARCM-M002116_seg001_r4_01.Labels
DistanceMap fire
DistanceMap type setValue 1
DistanceMap chamferMetric setValue 1
DistanceMap region setIndex 0 1
DistanceMap seed  setBoundingBox 1.74873 295.536 1.74873 295.536 0.453125 120.547
DistanceMap seed  setImmediate 0
DistanceMap seed  setOrtho 0
DistanceMap seed  showDragger 0
DistanceMap seed  showPoints 0
DistanceMap seed  setPointScale 1
DistanceMap seed  showOptionButton 1
DistanceMap seed  setNumPoints 1 -1 -1
DistanceMap seed  setValue 0 1.74873 1.74873 0.453125
DistanceMap fire
DistanceMap setViewerMask 16383
DistanceMap setPickable 1

set hideNewModules 0
[ {DistanceMap} create 0 ] setLabel {FruMARCM-M002116_seg001_r4_01.DistField}
FruMARCM-M002116_seg001_r4_01.DistField setIconPosition 20 527
FruMARCM-M002116_seg001_r4_01.DistField master connect DistanceMap result
FruMARCM-M002116_seg001_r4_01.DistField fire
FruMARCM-M002116_seg001_r4_01.DistField setViewerMask 16383

set hideNewModules 0
create HxArithmetic {Arithmetic}
Arithmetic setIconPosition 160 557
Arithmetic inputA connect FruMARCM-M002116_seg001_r4_01.DistField
Arithmetic fire
Arithmetic resultChannels setIndex 0 0
Arithmetic expr0 setState {a < 15}
Arithmetic expr1 setState 0
Arithmetic expr2 setState 0
Arithmetic expr3 setState 0
Arithmetic expr4 setState 0
Arithmetic expr5 setState 0
Arithmetic expr6 setState 0
Arithmetic expr7 setState 0
Arithmetic expr8 setState 0
Arithmetic resultType setValue 0
Arithmetic resultLocation setValue 0
Arithmetic resolution setMinMax 0 1 100000
Arithmetic resolution setValue 0 64
Arithmetic resolution setMinMax 1 1 100000
Arithmetic resolution setValue 1 64
Arithmetic resolution setMinMax 2 1 100000
Arithmetic resolution setValue 2 64
Arithmetic minBox setMinMax 0 -3.40282346638529e+38 3.40282346638529e+38
Arithmetic minBox setValue 0 -1
Arithmetic minBox setMinMax 1 -3.40282346638529e+38 3.40282346638529e+38
Arithmetic minBox setValue 1 -1
Arithmetic minBox setMinMax 2 -3.40282346638529e+38 3.40282346638529e+38
Arithmetic minBox setValue 2 -1
Arithmetic maxBox setMinMax 0 -3.40282346638529e+38 3.40282346638529e+38
Arithmetic maxBox setValue 0 1
Arithmetic maxBox setMinMax 1 -3.40282346638529e+38 3.40282346638529e+38
Arithmetic maxBox setValue 1 1
Arithmetic maxBox setMinMax 2 -3.40282346638529e+38 3.40282346638529e+38
Arithmetic maxBox setValue 2 1
Arithmetic options setValue 0 0
Arithmetic options setToggleVisible 0 1
Arithmetic fire
Arithmetic setViewerMask 16383
Arithmetic setPickable 1

set hideNewModules 0
[ {Arithmetic} create result ] setLabel {Result}
Result setIconPosition 53 577
Result master connect Arithmetic result
Result fire
Result setViewerMask 16383

set hideNewModules 0
create HxIsosurface {Isosurface}
Isosurface setIconPosition 401 589
Isosurface data connect Result
Isosurface colormap setDefaultColor 1 0.8 0.4
Isosurface colormap setDefaultAlpha 0.500000
Isosurface colormap setLocalRange 0
Isosurface colormap connect redblue.icol
Isosurface fire
Isosurface drawStyle setValue 1
Isosurface fire
Isosurface drawStyle setSpecularLighting 1
Isosurface drawStyle setTexture 0
Isosurface drawStyle setAlphaMode 1
Isosurface drawStyle setNormalBinding 1
Isosurface drawStyle setSortingMode 1
Isosurface drawStyle setLineWidth 0.000000
Isosurface drawStyle setOutlineColor 0 0 0.2
Isosurface textureWrap setIndex 0 1
Isosurface cullingMode setValue 0
Isosurface threshold setMinMax 0 1
Isosurface threshold setButtons 0
Isosurface threshold setIncrement 0.1
Isosurface threshold setValue 0
Isosurface threshold setSubMinMax 0 1
Isosurface options setValue 0 1
Isosurface options setToggleVisible 0 1
Isosurface options setValue 1 0
Isosurface options setToggleVisible 1 1
Isosurface resolution setMinMax 0 -2147483648 2147483648
Isosurface resolution setValue 0 2
Isosurface resolution setMinMax 1 -2147483648 2147483648
Isosurface resolution setValue 1 2
Isosurface resolution setMinMax 2 -2147483648 2147483648
Isosurface resolution setValue 2 2
Isosurface numberOfCores setMinMax 1 10
Isosurface numberOfCores setButtons 1
Isosurface numberOfCores setIncrement 1
Isosurface numberOfCores setValue 2
Isosurface numberOfCores setSubMinMax 1 10
{Isosurface} doIt hit
Isosurface fire
Isosurface setViewerMask 16382
Isosurface setShadowStyle 0
Isosurface setPickable 1

set hideNewModules 0
create HxCastField {CastField}
CastField setIconPosition 160 587
CastField data connect Result
CastField colormap setDefaultColor 1 0.8 0.5
CastField colormap setDefaultAlpha 0.500000
CastField colormap setLocalRange 0
CastField colormap connect redblue.icol
CastField fire
CastField outputType setIndex 0 6
CastField scaling setMinMax 0 -3.40282346638529e+38 3.40282346638529e+38
CastField scaling setValue 0 1
CastField scaling setMinMax 1 -3.40282346638529e+38 3.40282346638529e+38
CastField scaling setValue 1 0
CastField voxelGridOptions setValue 0 1
CastField voxelGridOptions setToggleVisible 0 1
CastField colorFieldOptions setIndex 0 0
CastField fire
CastField setViewerMask 16383
CastField setPickable 1

set hideNewModules 0
[ {CastField} create result ] setLabel {Result.Labelfield}
Result.Labelfield setIconPosition 20 617
Result.Labelfield master connect CastField result
Result.Labelfield fire
Result.Labelfield primary setIndex 0 0
Result.Labelfield fire
Result.Labelfield setViewerMask 16383

set hideNewModules 0
create HxOrthoSlice {OrthoSlice}
OrthoSlice setIconPosition 292 602
OrthoSlice data connect Result.Labelfield
{OrthoSlice} fire
OrthoSlice sliceOrientation setValue 0
{OrthoSlice} fire
OrthoSlice origin  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice origin  setImmediate 0
OrthoSlice origin  setOrtho 0
OrthoSlice origin  showDragger 0
OrthoSlice origin  showPoints 0
OrthoSlice origin  setPointScale 1
OrthoSlice origin  showOptionButton 1
OrthoSlice origin  setNumPoints 1 1 1
OrthoSlice origin  setValue 0 148.643 148.643 74.7969
OrthoSlice normal  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice normal  setImmediate 0
OrthoSlice normal  setOrtho 0
OrthoSlice normal  showDragger 0
OrthoSlice normal  showPoints 0
OrthoSlice normal  setPointScale 1
OrthoSlice normal  showOptionButton 1
OrthoSlice normal  setNumPoints 1 1 1
OrthoSlice normal  setValue 0 0 0 1
OrthoSlice options setValue 0 1
OrthoSlice options setToggleVisible 0 1
OrthoSlice options setValue 1 0
OrthoSlice options setToggleVisible 1 1
OrthoSlice options setValue 2 0
OrthoSlice options setToggleVisible 2 1
OrthoSlice mapping setIndex 0 1
OrthoSlice contrastLimit setMinMax 0 -2147483648 2147483648
OrthoSlice contrastLimit setValue 0 7
OrthoSlice colormap setDefaultColor 1 0.8 0.5
OrthoSlice colormap setDefaultAlpha 1.000000
OrthoSlice colormap setLocalRange 0
OrthoSlice colormap connect redblue.icol
OrthoSlice sliceNumber setMinMax 0 63
OrthoSlice sliceNumber setButtons 1
OrthoSlice sliceNumber setIncrement 1
OrthoSlice sliceNumber setValue 39
OrthoSlice sliceNumber setSubMinMax 0 63
OrthoSlice transparency setValue 0
OrthoSlice frameSettings setState item 0 1 item 2 1 color 3 1 0.5 0 
OrthoSlice fire

OrthoSlice fire
OrthoSlice setViewerMask 16382
OrthoSlice setShadowStyle 0
OrthoSlice setPickable 1

set hideNewModules 0
create HxGeneralizedHoughTransform {GeneralizedHoughTransform3D}
GeneralizedHoughTransform3D setIconPosition 231 659
GeneralizedHoughTransform3D TemplateShape connect FC6475_r4-filtered.surf
GeneralizedHoughTransform3D ImageGradients connect FruMARCM-M002116_seg001_r4_01.nrrd.Resampled
GeneralizedHoughTransform3D Mask connect Result.Labelfield
GeneralizedHoughTransform3D fire
GeneralizedHoughTransform3D RTableBins setMinMax 4 32
GeneralizedHoughTransform3D RTableBins setButtons 1
GeneralizedHoughTransform3D RTableBins setIncrement 1
GeneralizedHoughTransform3D RTableBins setValue 8
GeneralizedHoughTransform3D RTableBins setSubMinMax 4 32
GeneralizedHoughTransform3D Scale setState item 0 1 item 2 0.8 item 4 1.2 item 6 5 
GeneralizedHoughTransform3D XRotation setState item 0 1 item 2 10 item 4 5 
GeneralizedHoughTransform3D YRotation setState item 0 1 item 2 10 item 4 5 
GeneralizedHoughTransform3D ZRotation setState item 0 1 item 2 10 item 4 5 
GeneralizedHoughTransform3D InvertNormals setValue 0 1
GeneralizedHoughTransform3D InvertNormals setToggleVisible 0 1
GeneralizedHoughTransform3D Threads setMinMax 1 2
GeneralizedHoughTransform3D Threads setButtons 1
GeneralizedHoughTransform3D Threads setIncrement 1
GeneralizedHoughTransform3D Threads setValue 1
GeneralizedHoughTransform3D Threads setSubMinMax 1 2
GeneralizedHoughTransform3D fire
GeneralizedHoughTransform3D setViewerMask 16383
GeneralizedHoughTransform3D setPickable 1

set hideNewModules 0
[ {GeneralizedHoughTransform3D} create result ] setLabel {FruMARCM-M002116_seg001_r4_01.nrrd-accumulator}
FruMARCM-M002116_seg001_r4_01.nrrd-accumulator setIconPosition 20 688
FruMARCM-M002116_seg001_r4_01.nrrd-accumulator master connect GeneralizedHoughTransform3D result
FruMARCM-M002116_seg001_r4_01.nrrd-accumulator fire
FruMARCM-M002116_seg001_r4_01.nrrd-accumulator setViewerMask 16383

set hideNewModules 0
create HxDisplaySurface {SurfaceView}
SurfaceView setIconPosition 326 717
SurfaceView data connect FC6475_r4-filtered2.surf
SurfaceView colormap setDefaultColor 1 0.1 0.1
SurfaceView colormap setDefaultAlpha 0.500000
SurfaceView colormap setLocalRange 0
SurfaceView fire
SurfaceView drawStyle setValue 1
SurfaceView fire
SurfaceView drawStyle setSpecularLighting 1
SurfaceView drawStyle setTexture 1
SurfaceView drawStyle setAlphaMode 1
SurfaceView drawStyle setNormalBinding 0
SurfaceView drawStyle setSortingMode 1
SurfaceView drawStyle setLineWidth 0.000000
SurfaceView drawStyle setOutlineColor 0 0 0.2
SurfaceView textureWrap setIndex 0 1
SurfaceView cullingMode setValue 0
SurfaceView selectionMode setIndex 0 0
SurfaceView Patch setMinMax 0 2
SurfaceView Patch setButtons 1
SurfaceView Patch setIncrement 1
SurfaceView Patch setValue 0
SurfaceView Patch setSubMinMax 0 2
SurfaceView BoundaryId setIndex 0 -1
SurfaceView materials setIndex 0 1
SurfaceView materials setIndex 1 0
SurfaceView colorMode setIndex 0 0
SurfaceView baseTrans setMinMax 0 1
SurfaceView baseTrans setButtons 0
SurfaceView baseTrans setIncrement 0.1
SurfaceView baseTrans setValue 0.8
SurfaceView baseTrans setSubMinMax 0 1
SurfaceView VRMode setIndex 0 0
SurfaceView fire
SurfaceView hideBox 1
{SurfaceView} selectTriangles zab HIJMPLPPHPBEIMICFBDAAKEGMBCIBIAFKDGAHAABHLCAAGAALEOFJLBF
SurfaceView fire
SurfaceView setViewerMask 16383
SurfaceView setShadowStyle 0
SurfaceView setPickable 1

set hideNewModules 0
create HxOrthoSlice {OrthoSlice2}
OrthoSlice2 setIconPosition 393 742
OrthoSlice2 data connect FruMARCM-M002116_seg001_r4_01.nrrd-accumulator
{OrthoSlice2} fire
OrthoSlice2 sliceOrientation setValue 0
{OrthoSlice2} fire
OrthoSlice2 origin  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice2 origin  setImmediate 0
OrthoSlice2 origin  setOrtho 0
OrthoSlice2 origin  showDragger 0
OrthoSlice2 origin  showPoints 0
OrthoSlice2 origin  setPointScale 1
OrthoSlice2 origin  showOptionButton 1
OrthoSlice2 origin  setNumPoints 1 1 1
OrthoSlice2 origin  setValue 0 148.643 148.643 45.25
OrthoSlice2 normal  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice2 normal  setImmediate 0
OrthoSlice2 normal  setOrtho 0
OrthoSlice2 normal  showDragger 0
OrthoSlice2 normal  showPoints 0
OrthoSlice2 normal  setPointScale 1
OrthoSlice2 normal  showOptionButton 1
OrthoSlice2 normal  setNumPoints 1 1 1
OrthoSlice2 normal  setValue 0 0 0 1
OrthoSlice2 options setValue 0 0
OrthoSlice2 options setToggleVisible 0 1
OrthoSlice2 options setValue 1 0
OrthoSlice2 options setToggleVisible 1 1
OrthoSlice2 options setValue 2 0
OrthoSlice2 options setToggleVisible 2 1
OrthoSlice2 mapping setIndex 0 1
OrthoSlice2 contrastLimit setMinMax 0 -2147483648 2147483648
OrthoSlice2 contrastLimit setValue 0 7
OrthoSlice2 colormap setDefaultColor 1 0.8 0.5
OrthoSlice2 colormap setDefaultAlpha 1.000000
OrthoSlice2 colormap setLocalRange 0
OrthoSlice2 colormap connect redblue.icol
OrthoSlice2 sliceNumber setMinMax 0 59
OrthoSlice2 sliceNumber setButtons 1
OrthoSlice2 sliceNumber setIncrement 1
OrthoSlice2 sliceNumber setValue 22
OrthoSlice2 sliceNumber setSubMinMax 0 59
OrthoSlice2 transparency setValue 0
OrthoSlice2 frameSettings setState item 0 1 item 2 1 color 3 1 0.5 0 
OrthoSlice2 fire

OrthoSlice2 fire
OrthoSlice2 setViewerMask 16382
OrthoSlice2 setShadowStyle 0
OrthoSlice2 setPickable 1

set hideNewModules 0
create HxOrthoSlice {OrthoSlice3}
OrthoSlice3 setIconPosition 393 257
OrthoSlice3 data connect FruMARCM-M002116_seg001_r4_01.nrrd
{OrthoSlice3} fire
OrthoSlice3 sliceOrientation setValue 0
{OrthoSlice3} fire
OrthoSlice3 origin  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice3 origin  setImmediate 0
OrthoSlice3 origin  setOrtho 0
OrthoSlice3 origin  showDragger 0
OrthoSlice3 origin  showPoints 0
OrthoSlice3 origin  setPointScale 1
OrthoSlice3 origin  showOptionButton 1
OrthoSlice3 origin  setNumPoints 1 1 1
OrthoSlice3 origin  setValue 0 148.643 148.643 61
OrthoSlice3 normal  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice3 normal  setImmediate 0
OrthoSlice3 normal  setOrtho 0
OrthoSlice3 normal  showDragger 0
OrthoSlice3 normal  showPoints 0
OrthoSlice3 normal  setPointScale 1
OrthoSlice3 normal  showOptionButton 1
OrthoSlice3 normal  setNumPoints 1 1 1
OrthoSlice3 normal  setValue 0 0 0 1
OrthoSlice3 options setValue 0 0
OrthoSlice3 options setToggleVisible 0 1
OrthoSlice3 options setValue 1 0
OrthoSlice3 options setToggleVisible 1 1
OrthoSlice3 options setValue 2 0
OrthoSlice3 options setToggleVisible 2 1
OrthoSlice3 mapping setIndex 0 1
OrthoSlice3 contrastLimit setMinMax 0 -2147483648 2147483648
OrthoSlice3 contrastLimit setValue 0 7
OrthoSlice3 colormap setDefaultColor 1 0.8 0.5
OrthoSlice3 colormap setDefaultAlpha 1.000000
OrthoSlice3 colormap setLocalRange 1
OrthoSlice3 colormap setLocalMinMax 0.000000 2833.523438
OrthoSlice3 colormap connect grey.am
OrthoSlice3 sliceNumber setMinMax 0 121
OrthoSlice3 sliceNumber setButtons 1
OrthoSlice3 sliceNumber setIncrement 1
OrthoSlice3 sliceNumber setValue 61
OrthoSlice3 sliceNumber setSubMinMax 0 121
OrthoSlice3 transparency setValue 0
OrthoSlice3 frameSettings setState item 0 1 item 2 1 color 3 1 0.5 0 
OrthoSlice3 fire

OrthoSlice3 fire
OrthoSlice3 setViewerMask 16383
OrthoSlice3 setShadowStyle 0
OrthoSlice3 setPickable 1

set hideNewModules 0
create HxArbitraryCut {EmptyPlane}
EmptyPlane setIconPosition 394 407
EmptyPlane data connect FruMARCM-M002116_seg001_r4_01.nrrd.Resampled
EmptyPlane fire
EmptyPlane origin  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
EmptyPlane origin  setImmediate 0
EmptyPlane origin  setOrtho 0
EmptyPlane origin  showDragger 0
EmptyPlane origin  showPoints 0
EmptyPlane origin  setPointScale 1
EmptyPlane origin  showOptionButton 1
EmptyPlane origin  setNumPoints 1 1 1
EmptyPlane origin  setValue 0 148.643 148.643 61.5898
EmptyPlane normal  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
EmptyPlane normal  setImmediate 0
EmptyPlane normal  setOrtho 0
EmptyPlane normal  showDragger 0
EmptyPlane normal  showPoints 0
EmptyPlane normal  setPointScale 1
EmptyPlane normal  showOptionButton 1
EmptyPlane normal  setNumPoints 1 1 1
EmptyPlane normal  setValue 0 0 0 1
EmptyPlane frameSettings setState item 0 1 item 2 1 color 3 1 0.5 0 
EmptyPlane options setValue 0 0
EmptyPlane options setToggleVisible 0 1
EmptyPlane options setValue 1 0
EmptyPlane options setToggleVisible 1 1
EmptyPlane options setValue 2 1
EmptyPlane options setToggleVisible 2 1
EmptyPlane options setValue 3 0
EmptyPlane options setToggleVisible 3 1
EmptyPlane translate setMinMax 0 100
EmptyPlane translate setButtons 1
EmptyPlane translate setIncrement 1
EmptyPlane translate setValue 50.9091
EmptyPlane translate setSubMinMax 0 100
EmptyPlane orientation untouch
EmptyPlane setMinPlanePoint -1e+15 -1e+15 -1e+15
EmptyPlane setMaxPlanePoint 1e+15 1e+15 1e+15
EmptyPlane setTranslateRange 101.000000
EmptyPlane fire
EmptyPlane setViewerMask 16383
EmptyPlane setPickable 1

set hideNewModules 0
create HxVectors {Vectors}
Vectors setIconPosition 394 427
Vectors setLineWidth 1
Vectors setLogScale 0
Vectors data connect FruMARCM-M002116_seg001_r4_01.nrrd.Resampled
Vectors module connect EmptyPlane
Vectors colormap setDefaultColor 0.13 0.08 0.6
Vectors colormap setDefaultAlpha 0.500000
Vectors colormap setLocalRange 0
Vectors colormap enableAlpha 0
Vectors colormap enableAlphaToggle 1
Vectors fire
Vectors resolution setMinMax 0 -3.40282346638529e+38 3.40282346638529e+38
Vectors resolution setValue 0 50
Vectors resolution setMinMax 1 -3.40282346638529e+38 3.40282346638529e+38
Vectors resolution setValue 1 50
Vectors scale setMinMax 0 5
Vectors scale setButtons 0
Vectors scale setIncrement 0.333333
Vectors scale setValue 1
Vectors scale setSubMinMax 0 5
Vectors scaleZ setMinMax 0 1
Vectors scaleZ setButtons 0
Vectors scaleZ setIncrement 0.1
Vectors scaleZ setValue 1
Vectors scaleZ setSubMinMax 0 1
Vectors mode setValue 0 0
Vectors mode setToggleVisible 0 1
Vectors mode setValue 1 0
Vectors mode setToggleVisible 1 1
Vectors mode setValue 2 1
Vectors mode setToggleVisible 2 1
Vectors mode setValue 3 0
Vectors mode setToggleVisible 3 1
Vectors mode setValue 4 0
Vectors mode setToggleVisible 4 1
Vectors colorMode setIndex 0 0
Vectors phase setMinMax 0 360
Vectors phase setButtons 0
Vectors phase setIncrement 24
Vectors phase setValue 0
Vectors phase setSubMinMax 0 360
Vectors fire
Vectors setViewerMask 16382
Vectors setShadowStyle 0
Vectors setPickable 1

set hideNewModules 0
create HxScale {Scale}
Scale setIconPosition 434 527
Scale fire
Scale posX setMinMax 0.0500000007450581 0.949999988079071
Scale posX setButtons 1
Scale posX setIncrement 0.05
Scale posX setValue 0.05
Scale posX setSubMinMax 0.0500000007450581 0.949999988079071
Scale posY setMinMax 0.0500000007450581 0.949999988079071
Scale posY setButtons 1
Scale posY setIncrement 0.05
Scale posY setValue 0.05
Scale posY setSubMinMax 0.0500000007450581 0.949999988079071
Scale sizeX setMinMax 0.0500000007450581 0.899999976158142
Scale sizeX setButtons 1
Scale sizeX setIncrement 0.05
Scale sizeX setValue 0.243182
Scale sizeX setSubMinMax 0.0500000007450581 0.899999976158142
Scale sizeY setMinMax 0.0500000007450581 0.899999976158142
Scale sizeY setButtons 1
Scale sizeY setIncrement 0.05
Scale sizeY setValue 0.9
Scale sizeY setSubMinMax 0.0500000007450581 0.899999976158142
Scale frame setValue 0 1
Scale frame setToggleVisible 0 1
Scale frame setValue 1 0
Scale frame setToggleVisible 1 1
Scale frame setValue 2 0
Scale frame setToggleVisible 2 1
Scale ticks setValue 0 0
Scale ticks setToggleVisible 0 1
Scale ticks setValue 1 0
Scale ticks setToggleVisible 1 1
Scale ticks setValue 2 0
Scale ticks setToggleVisible 2 1
Scale subTicks setValue 0 0
Scale subTicks setToggleVisible 0 1
Scale subTicks setValue 1 0
Scale subTicks setToggleVisible 1 1
Scale unit setState mu
Scale Color setColor 0 1 1 1
Scale lineWidth setMinMax 0 1 10
Scale lineWidth setValue 0 1
Scale font setState name: {Lucida Grande} size: 14 bold: 0 italic: 0 color: 1 1 1
Scale Options setState item 0 0 
Scale xFactor setMinMax 0 100
Scale xFactor setButtons 0
Scale xFactor setIncrement 6.66667
Scale xFactor setValue 1
Scale xFactor setSubMinMax 0 100
Scale yFactor setMinMax 0 100
Scale yFactor setButtons 0
Scale yFactor setIncrement 6.66667
Scale yFactor setValue 1
Scale yFactor setSubMinMax 0 100
Scale setTextFormat "%.4g"
Scale fire
Scale setViewerMask 16383
Scale setPickable 1

set hideNewModules 0


viewer 0 setCameraOrientation 1 0 0 3.14159
viewer 0 setCameraPosition 225.615 95.9705 -394.98
viewer 0 setCameraFocalDistance 458.506
viewer 0 setCameraNearDistance 397.504
viewer 0 setCameraFarDistance 503.271
viewer 0 setCameraType orthographic
viewer 0 setCameraHeight 535.878
viewer 0 setAutoRedraw 1
viewer 0 redraw

