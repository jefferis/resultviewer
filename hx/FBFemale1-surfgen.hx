# Amira Script
remove -all
remove grey.am FBFemale1.nrrd FBFemale1.Resampled FBFemale1.Labels FBFemale1.surf FBFemale2.surf OrthoSlice Isosurface Resample LabelVoxel SurfaceGen SurfaceView SurfaceView2

# Create viewers
viewer setVertical 0

viewer 0 setBackgroundMode 1
viewer 0 setBackgroundColor 0.0980392 0.298039 0.298039
viewer 0 setBackgroundColor2 0.686275 0.701961 0.807843
viewer 0 setTransparencyType 5
viewer 0 setAutoRedraw 0
viewer 0 show
mainWindow show

set hideNewModules 1
[ load ${AMIRA_ROOT}/data/colormaps/grey.am ] setLabel grey.am
grey.am setIconPosition 0 0
grey.am setNoRemoveAll 1
grey.am fire
{grey.am} setMinMax 0 255
grey.am flags setValue 1
grey.am shift setMinMax -1 1
grey.am shift setButtons 0
grey.am shift setIncrement 0.133333
grey.am shift setValue 0
grey.am shift setSubMinMax -1 1
grey.am scale setMinMax 0 1
grey.am scale setButtons 0
grey.am scale setIncrement 0.1
grey.am scale setValue 1
grey.am scale setSubMinMax 0 1
grey.am fire
grey.am setViewerMask 16383

set hideNewModules 0
[ load /Volumes/JData/JPeople/Common/Neuroanatomy/ReferenceBrains/fullBrainFemale/FBFemale1.nrrd ] setLabel FBFemale1.nrrd
FBFemale1.nrrd setIconPosition 20 11
FBFemale1.nrrd fire
FBFemale1.nrrd setViewerMask 16383

set hideNewModules 0
[ load ${SCRIPTDIR}/FBFemale1-faces25k.surf ] setLabel FBFemale2.surf
FBFemale2.surf setIconPosition 23 181
FBFemale2.surf fire
FBFemale2.surf LevelOfDetail setMinMax -1 -1
FBFemale2.surf LevelOfDetail setButtons 1
FBFemale2.surf LevelOfDetail setIncrement 1
FBFemale2.surf LevelOfDetail setValue -1
FBFemale2.surf LevelOfDetail setSubMinMax -1 -1
FBFemale2.surf setTransform 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1
FBFemale2.surf fire
FBFemale2.surf setViewerMask 16383

set hideNewModules 0
create HxOrthoSlice {OrthoSlice}
OrthoSlice setIconPosition 284 11
OrthoSlice data connect FBFemale1.nrrd
{OrthoSlice} fire
OrthoSlice sliceOrientation setValue 0
{OrthoSlice} fire
OrthoSlice origin  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice origin  setImmediate 0
OrthoSlice origin  setOrtho 0
OrthoSlice origin  showDragger 0
OrthoSlice origin  showPoints 0
OrthoSlice origin  setPointScale 1
OrthoSlice origin  showOptionButton 1
OrthoSlice origin  setNumPoints 1 1 1
OrthoSlice origin  setValue 0 301.433 177.354 202
OrthoSlice normal  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice normal  setImmediate 0
OrthoSlice normal  setOrtho 0
OrthoSlice normal  showDragger 0
OrthoSlice normal  showPoints 0
OrthoSlice normal  setPointScale 1
OrthoSlice normal  showOptionButton 1
OrthoSlice normal  setNumPoints 1 1 1
OrthoSlice normal  setValue 0 0 0 1
OrthoSlice options setValue 0 1
OrthoSlice options setToggleVisible 0 1
OrthoSlice options setValue 1 0
OrthoSlice options setToggleVisible 1 1
OrthoSlice options setValue 2 0
OrthoSlice options setToggleVisible 2 1
OrthoSlice mapping setIndex 0 1
OrthoSlice contrastLimit setMinMax 0 -2147483648 2147483648
OrthoSlice contrastLimit setValue 0 7
OrthoSlice colormap setDefaultColor 1 0.8 0.5
OrthoSlice colormap setDefaultAlpha 1.000000
OrthoSlice colormap setLocalRange 1
OrthoSlice colormap setLocalMinMax 0.000000 14004.381836
OrthoSlice colormap connect grey.am
OrthoSlice sliceNumber setMinMax 0 202
OrthoSlice sliceNumber setButtons 1
OrthoSlice sliceNumber setIncrement 1
OrthoSlice sliceNumber setValue 202
OrthoSlice sliceNumber setSubMinMax 0 202
OrthoSlice transparency setValue 0
OrthoSlice frameSettings setState item 0 1 item 2 1 color 3 1 0.5 0 
OrthoSlice fire

OrthoSlice fire
OrthoSlice setViewerMask 16382
OrthoSlice setShadowStyle 0
OrthoSlice setPickable 1

set hideNewModules 0
create HxResample {Resample}
Resample setIconPosition 160 41
Resample data connect FBFemale1.nrrd
{Resample} fire
Resample filter setIndex 0 4
Resample mode setValue 1
Resample resolution setMinMax 0 1 2147483648
Resample resolution setValue 0 301.663391113281
Resample resolution setMinMax 1 1 2147483648
Resample resolution setValue 1 177.584732055664
Resample resolution setMinMax 2 1 2147483648
Resample resolution setValue 2 101.5
Resample voxelSize setMinMax 0 -3.40282346638529e+38 3.40282346638529e+38
Resample voxelSize setValue 0 2
Resample voxelSize setMinMax 1 -3.40282346638529e+38 3.40282346638529e+38
Resample voxelSize setValue 1 2
Resample voxelSize setMinMax 2 -3.40282346638529e+38 3.40282346638529e+38
Resample voxelSize setValue 2 2
Resample average setMinMax 0 1 32
Resample average setValue 0 2
Resample average setMinMax 1 1 32
Resample average setValue 1 2
Resample average setMinMax 2 1 32
Resample average setValue 2 1
Resample fire
Resample setViewerMask 16383
Resample setPickable 1

set hideNewModules 0
[ {Resample} create
 ] setLabel {FBFemale1.Resampled}
FBFemale1.Resampled setIconPosition 19 68
FBFemale1.Resampled master connect Resample result
FBFemale1.Resampled fire
FBFemale1.Resampled setViewerMask 16383

set hideNewModules 0
create HxIsosurface {Isosurface}
Isosurface setIconPosition 268 50
Isosurface data connect FBFemale1.Resampled
Isosurface colormap setDefaultColor 1 0.8 0.4
Isosurface colormap setDefaultAlpha 0.500000
Isosurface colormap setLocalRange 0
Isosurface fire
Isosurface drawStyle setValue 1
Isosurface fire
Isosurface drawStyle setSpecularLighting 1
Isosurface drawStyle setTexture 0
Isosurface drawStyle setAlphaMode 1
Isosurface drawStyle setNormalBinding 1
Isosurface drawStyle setSortingMode 1
Isosurface drawStyle setLineWidth 0.000000
Isosurface drawStyle setOutlineColor 0 0 0.2
Isosurface textureWrap setIndex 0 1
Isosurface cullingMode setValue 0
Isosurface threshold setMinMax 0 36148
Isosurface threshold setButtons 0
Isosurface threshold setIncrement 2409.87
Isosurface threshold setValue 4000
Isosurface threshold setSubMinMax 0 36148
Isosurface options setValue 0 1
Isosurface options setToggleVisible 0 1
Isosurface options setValue 1 0
Isosurface options setToggleVisible 1 1
Isosurface resolution setMinMax 0 -2147483648 2147483648
Isosurface resolution setValue 0 2
Isosurface resolution setMinMax 1 -2147483648 2147483648
Isosurface resolution setValue 1 2
Isosurface resolution setMinMax 2 -2147483648 2147483648
Isosurface resolution setValue 2 2
Isosurface numberOfCores setMinMax 1 10
Isosurface numberOfCores setButtons 1
Isosurface numberOfCores setIncrement 1
Isosurface numberOfCores setValue 2
Isosurface numberOfCores setSubMinMax 1 10
{Isosurface} doIt hit
Isosurface fire
Isosurface setViewerMask 16382
Isosurface setShadowStyle 0
Isosurface setPickable 1

set hideNewModules 0
create HxLabelVoxel {LabelVoxel}
LabelVoxel setIconPosition 233 95
LabelVoxel data connect FBFemale1.Resampled
LabelVoxel fire
LabelVoxel regions setState {Exterior  Brain}
LabelVoxel boundary01 setMinMax 0 36148
LabelVoxel boundary01 setButtons 1
LabelVoxel boundary01 setIncrement 10
LabelVoxel boundary01 setValue 3800
LabelVoxel boundary01 setSubMinMax 0 36148
LabelVoxel boundary12 setMinMax 0 36148
LabelVoxel boundary12 setButtons 1
LabelVoxel boundary12 setIncrement 10
LabelVoxel boundary12 setValue 970
LabelVoxel boundary12 setSubMinMax 0 36148
LabelVoxel boundary23 setMinMax 0 36148
LabelVoxel boundary23 setButtons 1
LabelVoxel boundary23 setIncrement 10
LabelVoxel boundary23 setValue 1100
LabelVoxel boundary23 setSubMinMax 0 36148
LabelVoxel boundary34 setMinMax 0 36148
LabelVoxel boundary34 setButtons 1
LabelVoxel boundary34 setIncrement 10
LabelVoxel boundary34 setValue 2000
LabelVoxel boundary34 setSubMinMax 0 36148
LabelVoxel options setValue 0 1
LabelVoxel options setToggleVisible 0 1
LabelVoxel options setValue 1 0
LabelVoxel options setToggleVisible 1 1
LabelVoxel options setValue 2 0
LabelVoxel options setToggleVisible 2 1
LabelVoxel fire
LabelVoxel setViewerMask 16383
LabelVoxel setPickable 1

set hideNewModules 0
[ {LabelVoxel} create
 ] setLabel {FBFemale1.Labels}
FBFemale1.Labels setIconPosition 19 88
FBFemale1.Labels master connect LabelVoxel result
FBFemale1.Labels ImageData connect FBFemale1.Resampled
FBFemale1.Labels fire
FBFemale1.Labels primary setIndex 0 0
FBFemale1.Labels fire
FBFemale1.Labels setViewerMask 16383

set hideNewModules 0
create HxGMC {SurfaceGen}
SurfaceGen setIconPosition 225 122
SurfaceGen data connect FBFemale1.Labels
SurfaceGen fire
SurfaceGen smoothing setIndex 0 3
SurfaceGen options setValue 0 1
SurfaceGen options setToggleVisible 0 1
SurfaceGen options setValue 1 0
SurfaceGen options setToggleVisible 1 1
SurfaceGen border setValue 0 1
SurfaceGen border setToggleVisible 0 1
SurfaceGen border setValue 1 0
SurfaceGen border setToggleVisible 1 1
SurfaceGen minEdgeLength setMinMax 0 0 0.800000011920929
SurfaceGen minEdgeLength setValue 0 0
SurfaceGen materialList setIndex 0 0
SurfaceGen fire
SurfaceGen setViewerMask 16383
SurfaceGen setPickable 1

set hideNewModules 0
[ {SurfaceGen} create {FBFemale1.surf}
 ] setLabel {FBFemale1.surf}
FBFemale1.surf setIconPosition 20 139
FBFemale1.surf master connect SurfaceGen result
FBFemale1.surf fire
FBFemale1.surf LevelOfDetail setMinMax -1 -1
FBFemale1.surf LevelOfDetail setButtons 1
FBFemale1.surf LevelOfDetail setIncrement 1
FBFemale1.surf LevelOfDetail setValue -1
FBFemale1.surf LevelOfDetail setSubMinMax -1 -1
FBFemale1.surf setTransform 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1
FBFemale1.surf fire
FBFemale1.surf setViewerMask 16383

set hideNewModules 0
create HxDisplaySurface {SurfaceView}
SurfaceView setIconPosition 241 158
SurfaceView data connect FBFemale1.surf
SurfaceView colormap setDefaultColor 1 0.1 0.1
SurfaceView colormap setDefaultAlpha 0.500000
SurfaceView colormap setLocalRange 0
SurfaceView fire
SurfaceView drawStyle setValue 1
SurfaceView fire
SurfaceView drawStyle setSpecularLighting 1
SurfaceView drawStyle setTexture 1
SurfaceView drawStyle setAlphaMode 1
SurfaceView drawStyle setNormalBinding 0
SurfaceView drawStyle setSortingMode 1
SurfaceView drawStyle setLineWidth 0.000000
SurfaceView drawStyle setOutlineColor 0 0 0.2
SurfaceView textureWrap setIndex 0 1
SurfaceView cullingMode setValue 0
SurfaceView selectionMode setIndex 0 0
SurfaceView Patch setMinMax 0 1
SurfaceView Patch setButtons 1
SurfaceView Patch setIncrement 1
SurfaceView Patch setValue 0
SurfaceView Patch setSubMinMax 0 1
SurfaceView BoundaryId setIndex 0 -1
SurfaceView materials setIndex 0 1
SurfaceView materials setIndex 1 0
SurfaceView colorMode setIndex 0 0
SurfaceView baseTrans setMinMax 0 1
SurfaceView baseTrans setButtons 0
SurfaceView baseTrans setIncrement 0.1
SurfaceView baseTrans setValue 0.8
SurfaceView baseTrans setSubMinMax 0 1
SurfaceView VRMode setIndex 0 0
SurfaceView fire
SurfaceView hideBox 1
{SurfaceView} selectTriangles zab HIJMONMOABAJAACABAAEMBADADBIMJPOKJMOBGDOMCEMIBNNBGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPOJEKMOJAFAAAAAAAAAAAAAAAAAAAAIAHBOHEBCDNJLNAPMCNOMF
SurfaceView fire
SurfaceView setViewerMask 16382
SurfaceView setShadowStyle 0
SurfaceView setPickable 1

set hideNewModules 0
create HxDisplaySurface {SurfaceView2}
SurfaceView2 setIconPosition 232 182
SurfaceView2 data connect FBFemale2.surf
SurfaceView2 colormap setDefaultColor 1 0.1 0.1
SurfaceView2 colormap setDefaultAlpha 0.500000
SurfaceView2 colormap setLocalRange 0
SurfaceView2 fire
SurfaceView2 drawStyle setValue 1
SurfaceView2 fire
SurfaceView2 drawStyle setSpecularLighting 1
SurfaceView2 drawStyle setTexture 1
SurfaceView2 drawStyle setAlphaMode 1
SurfaceView2 drawStyle setNormalBinding 0
SurfaceView2 drawStyle setSortingMode 1
SurfaceView2 drawStyle setLineWidth 0.000000
SurfaceView2 drawStyle setOutlineColor 0 0 0.2
SurfaceView2 textureWrap setIndex 0 1
SurfaceView2 cullingMode setValue 0
SurfaceView2 selectionMode setIndex 0 0
SurfaceView2 Patch setMinMax 0 1
SurfaceView2 Patch setButtons 1
SurfaceView2 Patch setIncrement 1
SurfaceView2 Patch setValue 0
SurfaceView2 Patch setSubMinMax 0 1
SurfaceView2 BoundaryId setIndex 0 -1
SurfaceView2 materials setIndex 0 1
SurfaceView2 materials setIndex 1 0
SurfaceView2 colorMode setIndex 0 0
SurfaceView2 baseTrans setMinMax 0 1
SurfaceView2 baseTrans setButtons 0
SurfaceView2 baseTrans setIncrement 0.1
SurfaceView2 baseTrans setValue 0.8
SurfaceView2 baseTrans setSubMinMax 0 1
SurfaceView2 VRMode setIndex 0 0
SurfaceView2 fire
SurfaceView2 hideBox 1
{SurfaceView2} selectTriangles zab HIJMONMBABABAAAAAIACKAPOJPDGAPPIAAEIAAAAAALGKLAHONPBCIIB
SurfaceView2 fire
SurfaceView2 setViewerMask 16383
SurfaceView2 setShadowStyle 0
SurfaceView2 setPickable 1

set hideNewModules 0


viewer 0 setCameraOrientation 0.999218 -0.0235744 -0.0317593 3.65421
viewer 0 setCameraPosition 271.782 437.871 -575.768
viewer 0 setCameraFocalDistance 705.021
viewer 0 setCameraNearDistance 562.319
viewer 0 setCameraFarDistance 857.513
viewer 0 setCameraType perspective
viewer 0 setCameraHeightAngle 44.9023
viewer 0 setAutoRedraw 1
viewer 0 redraw

