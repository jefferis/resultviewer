# Amira Script
remove -all
remove grey.am FBFemale1.nrrd FBFemale2.Resampled FBFemale2.Labels.am FBFemale2.DistField FBFemale2.to-short FBFemale3.Labels FBFemale3.surf OrthoSlice Resample LabelVoxel2 OrthoSlice3 DistanceTransform Isosurface2 CastField LabelVoxel3 SurfaceGen2 OrthoSlice4 SurfaceView3

# Create viewers
viewer setVertical 0

viewer 0 setBackgroundMode 1
viewer 0 setBackgroundColor 0.0980392 0.298039 0.298039
viewer 0 setBackgroundColor2 0.686275 0.701961 0.807843
viewer 0 setTransparencyType 5
viewer 0 setAutoRedraw 0
viewer 0 show
mainWindow show

set hideNewModules 1
[ load ${AMIRA_ROOT}/data/colormaps/grey.am ] setLabel grey.am
grey.am setIconPosition 0 0
grey.am setNoRemoveAll 1
grey.am fire
{grey.am} setMinMax 0 255
grey.am flags setValue 1
grey.am shift setMinMax -1 1
grey.am shift setButtons 0
grey.am shift setIncrement 0.133333
grey.am shift setValue 0
grey.am shift setSubMinMax -1 1
grey.am scale setMinMax 0 1
grey.am scale setButtons 0
grey.am scale setIncrement 0.1
grey.am scale setValue 1
grey.am scale setSubMinMax 0 1
grey.am fire
grey.am setViewerMask 16383

set hideNewModules 0
[ load /Volumes/JData/JPeople/Common/Neuroanatomy/ReferenceBrains/fullBrainFemale/FBFemale1.nrrd ] setLabel FBFemale1.nrrd
FBFemale1.nrrd setIconPosition 15 63
FBFemale1.nrrd fire
FBFemale1.nrrd setViewerMask 16383

set hideNewModules 0
[ load ${SCRIPTDIR}/FBFemale1-dilate40u-maxDist8u-files/FBFemale2.Labels.am ] setLabel FBFemale2.Labels.am
FBFemale2.Labels.am setIconPosition 6 123
FBFemale2.Labels.am fire
FBFemale2.Labels.am primary setIndex 0 0
FBFemale2.Labels.am fire
FBFemale2.Labels.am setViewerMask 16383

set hideNewModules 0
[ load ${SCRIPTDIR}/FBFemale1-dilate40u-maxDist8u.surf ] setLabel FBFemale3.surf
FBFemale3.surf setIconPosition 10 356
FBFemale3.surf fire
FBFemale3.surf LevelOfDetail setMinMax -1 -1
FBFemale3.surf LevelOfDetail setButtons 1
FBFemale3.surf LevelOfDetail setIncrement 1
FBFemale3.surf LevelOfDetail setValue -1
FBFemale3.surf LevelOfDetail setSubMinMax -1 -1
FBFemale3.surf setTransform 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1
FBFemale3.surf fire
FBFemale3.surf setViewerMask 16383

set hideNewModules 0
create HxOrthoSlice {OrthoSlice}
OrthoSlice setIconPosition 274 50
OrthoSlice data connect FBFemale1.nrrd
{OrthoSlice} fire
OrthoSlice sliceOrientation setValue 0
{OrthoSlice} fire
OrthoSlice origin  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice origin  setImmediate 0
OrthoSlice origin  setOrtho 0
OrthoSlice origin  showDragger 0
OrthoSlice origin  showPoints 0
OrthoSlice origin  setPointScale 1
OrthoSlice origin  showOptionButton 1
OrthoSlice origin  setNumPoints 1 1 1
OrthoSlice origin  setValue 0 301.433 177.354 86
OrthoSlice normal  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice normal  setImmediate 0
OrthoSlice normal  setOrtho 0
OrthoSlice normal  showDragger 0
OrthoSlice normal  showPoints 0
OrthoSlice normal  setPointScale 1
OrthoSlice normal  showOptionButton 1
OrthoSlice normal  setNumPoints 1 1 1
OrthoSlice normal  setValue 0 0 0 1
OrthoSlice options setValue 0 1
OrthoSlice options setValue 1 0
OrthoSlice options setValue 2 0
OrthoSlice mapping setIndex 0 1
OrthoSlice contrastLimit setMinMax 0 -2147483648 2147483648
OrthoSlice contrastLimit setValue 0 7
OrthoSlice colormap setDefaultColor 1 0.8 0.5
OrthoSlice colormap setDefaultAlpha 1.000000
OrthoSlice colormap setLocalRange 1
OrthoSlice colormap setLocalMinMax 0.000000 14004.381836
OrthoSlice colormap connect grey.am
OrthoSlice sliceNumber setMinMax 0 202
OrthoSlice sliceNumber setButtons 1
OrthoSlice sliceNumber setIncrement 1
OrthoSlice sliceNumber setValue 86.3091
OrthoSlice sliceNumber setSubMinMax 0 202
OrthoSlice transparency setValue 0
OrthoSlice frameSettings setState item 0 1 item 2 1 color 3 1 0.5 0 
OrthoSlice fire

OrthoSlice fire
OrthoSlice setViewerMask 16382
OrthoSlice setShadowStyle 0
OrthoSlice setPickable 1

set hideNewModules 0
create HxResample {Resample}
Resample setIconPosition 181 71
Resample data connect FBFemale1.nrrd
{Resample} fire
Resample filter setIndex 0 4
Resample mode setValue 1
Resample resolution setMinMax 0 1 2147483648
Resample resolution setValue 0 150.831695556641
Resample resolution setMinMax 1 1 2147483648
Resample resolution setValue 1 88.792366027832
Resample resolution setMinMax 2 1 2147483648
Resample resolution setValue 2 50.75
Resample voxelSize setMinMax 0 -3.40282346638529e+38 3.40282346638529e+38
Resample voxelSize setValue 0 4
Resample voxelSize setMinMax 1 -3.40282346638529e+38 3.40282346638529e+38
Resample voxelSize setValue 1 4
Resample voxelSize setMinMax 2 -3.40282346638529e+38 3.40282346638529e+38
Resample voxelSize setValue 2 4
Resample average setMinMax 0 1 32
Resample average setValue 0 2
Resample average setMinMax 1 1 32
Resample average setValue 1 2
Resample average setMinMax 2 1 32
Resample average setValue 2 1
Resample fire
Resample setViewerMask 16383
Resample setPickable 1

set hideNewModules 0
[ {Resample} create
 ] setLabel {FBFemale2.Resampled}
FBFemale2.Resampled setIconPosition 5 95
FBFemale2.Resampled master connect Resample result
FBFemale2.Resampled fire
FBFemale2.Resampled setViewerMask 16383

set hideNewModules 0
create HxLabelVoxel {LabelVoxel2}
LabelVoxel2 setIconPosition 234 111
LabelVoxel2 data connect FBFemale2.Resampled
LabelVoxel2 fire
LabelVoxel2 regions setState {Exterior  Brain}
LabelVoxel2 boundary01 setMinMax 0 35301
LabelVoxel2 boundary01 setButtons 1
LabelVoxel2 boundary01 setIncrement 10
LabelVoxel2 boundary01 setValue 3800
LabelVoxel2 boundary01 setSubMinMax 0 35301
LabelVoxel2 boundary12 setMinMax 0 35301
LabelVoxel2 boundary12 setButtons 1
LabelVoxel2 boundary12 setIncrement 10
LabelVoxel2 boundary12 setValue 970
LabelVoxel2 boundary12 setSubMinMax 0 35301
LabelVoxel2 boundary23 setMinMax 0 35301
LabelVoxel2 boundary23 setButtons 1
LabelVoxel2 boundary23 setIncrement 10
LabelVoxel2 boundary23 setValue 1100
LabelVoxel2 boundary23 setSubMinMax 0 35301
LabelVoxel2 boundary34 setMinMax 0 35301
LabelVoxel2 boundary34 setButtons 1
LabelVoxel2 boundary34 setIncrement 10
LabelVoxel2 boundary34 setValue 2000
LabelVoxel2 boundary34 setSubMinMax 0 35301
LabelVoxel2 options setValue 0 1
LabelVoxel2 options setValue 1 0
LabelVoxel2 options setValue 2 0
LabelVoxel2 fire
LabelVoxel2 setViewerMask 16383
LabelVoxel2 setPickable 1

set hideNewModules 0
create HxOrthoSlice {OrthoSlice3}
OrthoSlice3 setIconPosition 228 140
OrthoSlice3 data connect FBFemale2.Labels.am
{OrthoSlice3} fire
OrthoSlice3 sliceOrientation setValue 0
{OrthoSlice3} fire
OrthoSlice3 origin  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice3 origin  setImmediate 0
OrthoSlice3 origin  setOrtho 0
OrthoSlice3 origin  showDragger 0
OrthoSlice3 origin  showPoints 0
OrthoSlice3 origin  setPointScale 1
OrthoSlice3 origin  showOptionButton 1
OrthoSlice3 origin  setNumPoints 1 1 1
OrthoSlice3 origin  setValue 0 301.433 177.354 -291
OrthoSlice3 normal  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice3 normal  setImmediate 0
OrthoSlice3 normal  setOrtho 0
OrthoSlice3 normal  showDragger 0
OrthoSlice3 normal  showPoints 0
OrthoSlice3 normal  setPointScale 1
OrthoSlice3 normal  showOptionButton 1
OrthoSlice3 normal  setNumPoints 1 1 1
OrthoSlice3 normal  setValue 0 0 0 1
OrthoSlice3 options setValue 0 0
OrthoSlice3 options setValue 1 0
OrthoSlice3 options setValue 2 0
OrthoSlice3 mapping setIndex 0 1
OrthoSlice3 contrastLimit setMinMax 0 -2147483648 2147483648
OrthoSlice3 contrastLimit setValue 0 7
OrthoSlice3 colormap setDefaultColor 1 0.8 0.5
OrthoSlice3 colormap setDefaultAlpha 1.000000
OrthoSlice3 colormap setLocalRange 1
OrthoSlice3 colormap setLocalMinMax 0.000000 1.000000
OrthoSlice3 colormap connect grey.am
OrthoSlice3 sliceNumber setMinMax 0 250
OrthoSlice3 sliceNumber setButtons 1
OrthoSlice3 sliceNumber setIncrement 1
OrthoSlice3 sliceNumber setValue 27
OrthoSlice3 sliceNumber setSubMinMax 0 250
OrthoSlice3 transparency setValue 0
OrthoSlice3 frameSettings setState item 0 1 item 2 1 color 3 1 0.5 0 
OrthoSlice3 fire

OrthoSlice3 fire
OrthoSlice3 setViewerMask 16382
OrthoSlice3 setShadowStyle 0
OrthoSlice3 setPickable 1

set hideNewModules 0
create HxDistanceTransform {DistanceTransform}
DistanceTransform setIconPosition 154 170
DistanceTransform data connect FBFemale2.Labels.am
DistanceTransform fire
DistanceTransform region setIndex 0 1
DistanceTransform options setIndex 0 2
DistanceTransform fire
DistanceTransform setViewerMask 16383
DistanceTransform setPickable 1

set hideNewModules 0
[ {DistanceTransform} create 0 ] setLabel {FBFemale2.DistField}
FBFemale2.DistField setIconPosition 6 197
FBFemale2.DistField master connect DistanceTransform result
FBFemale2.DistField fire
FBFemale2.DistField setViewerMask 16383

set hideNewModules 0
create HxIsosurface {Isosurface2}
Isosurface2 setIconPosition 262 208
Isosurface2 data connect FBFemale2.DistField
Isosurface2 colormap setDefaultColor 1 0.8 0.4
Isosurface2 colormap setDefaultAlpha 0.500000
Isosurface2 colormap setLocalRange 0
Isosurface2 fire
Isosurface2 drawStyle setValue 1
Isosurface2 fire
Isosurface2 drawStyle setSpecularLighting 1
Isosurface2 drawStyle setTexture 0
Isosurface2 drawStyle setAlphaMode 1
Isosurface2 drawStyle setNormalBinding 1
Isosurface2 drawStyle setSortingMode 1
Isosurface2 drawStyle setLineWidth 0.000000
Isosurface2 drawStyle setOutlineColor 0 0 0.2
Isosurface2 textureWrap setIndex 0 1
Isosurface2 cullingMode setValue 0
Isosurface2 threshold setMinMax 0 887.188842773438
Isosurface2 threshold setButtons 0
Isosurface2 threshold setIncrement 59.1459
Isosurface2 threshold setValue 40
Isosurface2 threshold setSubMinMax 0 887.188842773438
Isosurface2 options setValue 0 1
Isosurface2 options setValue 1 0
Isosurface2 resolution setMinMax 0 -2147483648 2147483648
Isosurface2 resolution setValue 0 2
Isosurface2 resolution setMinMax 1 -2147483648 2147483648
Isosurface2 resolution setValue 1 2
Isosurface2 resolution setMinMax 2 -2147483648 2147483648
Isosurface2 resolution setValue 2 2
Isosurface2 numberOfCores setMinMax 1 10
Isosurface2 numberOfCores setButtons 1
Isosurface2 numberOfCores setIncrement 1
Isosurface2 numberOfCores setValue 2
Isosurface2 numberOfCores setSubMinMax 1 10
Isosurface2 doIt snap 0 1
{Isosurface2} doIt hit
Isosurface2 fire
Isosurface2 setViewerMask 16382
Isosurface2 setShadowStyle 0
Isosurface2 setPickable 1

set hideNewModules 0
create HxCastField {CastField}
CastField setIconPosition 154 227
CastField data connect FBFemale2.DistField
CastField colormap setDefaultColor 1 0.8 0.5
CastField colormap setDefaultAlpha 0.500000
CastField colormap setLocalRange 0
CastField fire
CastField outputType setIndex 0 1
CastField scaling setMinMax 0 -3.40282346638529e+38 3.40282346638529e+38
CastField scaling setValue 0 -10
CastField scaling setMinMax 1 -3.40282346638529e+38 3.40282346638529e+38
CastField scaling setValue 1 0
CastField voxelGridOptions setValue 0 1
CastField colorFieldOptions setIndex 0 0
CastField fire
CastField setViewerMask 16383
CastField select
CastField scaling setPin 1
CastField deselect
CastField setPickable 1

set hideNewModules 0
[ {CastField} create result ] setLabel {FBFemale2.to-short}
FBFemale2.to-short setIconPosition 14 257
FBFemale2.to-short master connect CastField result
FBFemale2.to-short fire
FBFemale2.to-short setViewerMask 16383

set hideNewModules 0
create HxLabelVoxel {LabelVoxel3}
LabelVoxel3 setIconPosition 232 273
LabelVoxel3 data connect FBFemale2.to-short
LabelVoxel3 fire
LabelVoxel3 regions setState {Exterior Bone}
LabelVoxel3 boundary01 setMinMax -8871 0
LabelVoxel3 boundary01 setButtons 1
LabelVoxel3 boundary01 setIncrement 10
LabelVoxel3 boundary01 setValue -400
LabelVoxel3 boundary01 setSubMinMax -8871 0
LabelVoxel3 boundary12 setMinMax -8871 0
LabelVoxel3 boundary12 setButtons 1
LabelVoxel3 boundary12 setIncrement 10
LabelVoxel3 boundary12 setValue 0
LabelVoxel3 boundary12 setSubMinMax -8871 0
LabelVoxel3 boundary23 setMinMax -8871 0
LabelVoxel3 boundary23 setButtons 1
LabelVoxel3 boundary23 setIncrement 10
LabelVoxel3 boundary23 setValue 0
LabelVoxel3 boundary23 setSubMinMax -8871 0
LabelVoxel3 boundary34 setMinMax -8871 0
LabelVoxel3 boundary34 setButtons 1
LabelVoxel3 boundary34 setIncrement 10
LabelVoxel3 boundary34 setValue 0
LabelVoxel3 boundary34 setSubMinMax -8871 0
LabelVoxel3 options setValue 0 1
LabelVoxel3 options setValue 1 0
LabelVoxel3 options setValue 2 0
LabelVoxel3 fire
LabelVoxel3 setViewerMask 16383
LabelVoxel3 select
LabelVoxel3 boundary01 setPin 1
LabelVoxel3 deselect
LabelVoxel3 setPickable 1

set hideNewModules 0
[ {LabelVoxel3} create
 ] setLabel {FBFemale3.Labels}
FBFemale3.Labels setIconPosition 14 277
FBFemale3.Labels master connect LabelVoxel3 result
FBFemale3.Labels ImageData connect FBFemale2.to-short
FBFemale3.Labels fire
FBFemale3.Labels primary setIndex 0 0
FBFemale3.Labels fire
FBFemale3.Labels setViewerMask 16383

set hideNewModules 0
create HxGMC {SurfaceGen2}
SurfaceGen2 setIconPosition 162 314
SurfaceGen2 data connect FBFemale3.Labels
SurfaceGen2 fire
SurfaceGen2 smoothing setIndex 0 1
SurfaceGen2 options setValue 0 1
SurfaceGen2 options setValue 1 0
SurfaceGen2 border setValue 0 1
SurfaceGen2 border setValue 1 0
SurfaceGen2 minEdgeLength setMinMax 0 0 0.800000011920929
SurfaceGen2 minEdgeLength setValue 0 0
SurfaceGen2 materialList setIndex 0 0
SurfaceGen2 fire
SurfaceGen2 setViewerMask 16383
SurfaceGen2 setPickable 1

set hideNewModules 0
create HxOrthoSlice {OrthoSlice4}
OrthoSlice4 setIconPosition 272 297
OrthoSlice4 data connect FBFemale3.Labels
{OrthoSlice4} fire
OrthoSlice4 sliceOrientation setValue 0
{OrthoSlice4} fire
OrthoSlice4 origin  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice4 origin  setImmediate 0
OrthoSlice4 origin  setOrtho 0
OrthoSlice4 origin  showDragger 0
OrthoSlice4 origin  showPoints 0
OrthoSlice4 origin  setPointScale 1
OrthoSlice4 origin  showOptionButton 1
OrthoSlice4 origin  setNumPoints 1 1 1
OrthoSlice4 origin  setValue 0 301.433 177.354 101
OrthoSlice4 normal  setBoundingBox -1e+08 1e+08 -1e+08 1e+08 -1e+08 1e+08
OrthoSlice4 normal  setImmediate 0
OrthoSlice4 normal  setOrtho 0
OrthoSlice4 normal  showDragger 0
OrthoSlice4 normal  showPoints 0
OrthoSlice4 normal  setPointScale 1
OrthoSlice4 normal  showOptionButton 1
OrthoSlice4 normal  setNumPoints 1 1 1
OrthoSlice4 normal  setValue 0 0 0 1
OrthoSlice4 options setValue 0 0
OrthoSlice4 options setValue 1 0
OrthoSlice4 options setValue 2 0
OrthoSlice4 mapping setIndex 0 1
OrthoSlice4 contrastLimit setMinMax 0 -2147483648 2147483648
OrthoSlice4 contrastLimit setValue 0 7
OrthoSlice4 colormap setDefaultColor 1 0.8 0.5
OrthoSlice4 colormap setDefaultAlpha 1.000000
OrthoSlice4 colormap setLocalRange 1
OrthoSlice4 colormap setLocalMinMax 0.000000 1.000000
OrthoSlice4 colormap connect grey.am
OrthoSlice4 sliceNumber setMinMax 0 250
OrthoSlice4 sliceNumber setButtons 1
OrthoSlice4 sliceNumber setIncrement 1
OrthoSlice4 sliceNumber setValue 125
OrthoSlice4 sliceNumber setSubMinMax 0 250
OrthoSlice4 transparency setValue 0
OrthoSlice4 frameSettings setState item 0 1 item 2 1 color 3 1 0.5 0 
OrthoSlice4 fire

OrthoSlice4 fire
OrthoSlice4 setViewerMask 16382
OrthoSlice4 setShadowStyle 0
OrthoSlice4 setPickable 1

set hideNewModules 0
create HxDisplaySurface {SurfaceView3}
SurfaceView3 setIconPosition 251 357
SurfaceView3 data connect FBFemale3.surf
SurfaceView3 colormap setDefaultColor 1 0.1 0.1
SurfaceView3 colormap setDefaultAlpha 0.500000
SurfaceView3 colormap setLocalRange 0
SurfaceView3 fire
SurfaceView3 drawStyle setValue 0
SurfaceView3 fire
SurfaceView3 drawStyle setSpecularLighting 1
SurfaceView3 drawStyle setTexture 1
SurfaceView3 drawStyle setAlphaMode 1
SurfaceView3 drawStyle setNormalBinding 0
SurfaceView3 drawStyle setSortingMode 1
SurfaceView3 drawStyle setLineWidth 0.000000
SurfaceView3 drawStyle setOutlineColor 0 0 0.2
SurfaceView3 textureWrap setIndex 0 1
SurfaceView3 cullingMode setValue 0
SurfaceView3 selectionMode setIndex 0 0
SurfaceView3 Patch setMinMax 0 1
SurfaceView3 Patch setButtons 1
SurfaceView3 Patch setIncrement 1
SurfaceView3 Patch setValue 0
SurfaceView3 Patch setSubMinMax 0 1
SurfaceView3 BoundaryId setIndex 0 -1
SurfaceView3 materials setIndex 0 1
SurfaceView3 materials setIndex 1 0
SurfaceView3 colorMode setIndex 0 0
SurfaceView3 baseTrans setMinMax 0 1
SurfaceView3 baseTrans setButtons 0
SurfaceView3 baseTrans setIncrement 0.1
SurfaceView3 baseTrans setValue 0.8
SurfaceView3 baseTrans setSubMinMax 0 1
SurfaceView3 VRMode setIndex 0 0
SurfaceView3 fire
SurfaceView3 hideBox 1
{SurfaceView3} selectTriangles zab HIJMONNIEBANAAEAAIAELBHFIAHPLFFMEOADCBHMFKAFPDJOGOAAAAAAAADGCEKJOLAGAANIJGFMBHAAAAAAAAMMPMLPPBAANLBMBNDD
SurfaceView3 fire
SurfaceView3 setViewerMask 16383
SurfaceView3 setShadowStyle 0
SurfaceView3 setPickable 1

set hideNewModules 0

temperature.icol clipGeom OrthoSlice3
physics.icol clipGeom OrthoSlice3
seismic.col clipGeom OrthoSlice3
glow.col clipGeom OrthoSlice3
volrenRed.col clipGeom OrthoSlice3
volrenGreen.col clipGeom OrthoSlice3
volrenGlow.am clipGeom OrthoSlice3
grey.am clipGeom OrthoSlice3
FBFemale1.nrrd clipGeom OrthoSlice3
FBFemale2.Resampled clipGeom OrthoSlice3
FBFemale2.Labels.am clipGeom OrthoSlice3
FBFemale2.DistField clipGeom OrthoSlice3
Settings clipGeom OrthoSlice3
Data clipGeom OrthoSlice3
Display clipGeom OrthoSlice3
Compute clipGeom OrthoSlice3
Animation-Demo-Scripts clipGeom OrthoSlice3
Colormaps clipGeom OrthoSlice3
OrthoSlice clipGeom OrthoSlice3
Resample clipGeom OrthoSlice3
LabelVoxel2 clipGeom OrthoSlice3
OrthoSlice3 invertClippingPlane
DistanceTransform clipGeom OrthoSlice3
Isosurface2 clipGeom OrthoSlice3

viewer 0 setCameraOrientation 0.974564 0.219644 -0.0445187 3.84207
viewer 0 setCameraPosition 132.91 638.134 -482.991
viewer 0 setCameraFocalDistance 754.222
viewer 0 setCameraNearDistance 499.396
viewer 0 setCameraFarDistance 1009.56
viewer 0 setCameraType perspective
viewer 0 setCameraHeightAngle 44.9023
viewer 0 setAutoRedraw 1
viewer 0 redraw

