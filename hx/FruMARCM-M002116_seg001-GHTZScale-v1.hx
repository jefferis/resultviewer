# Amira Script
remove -all
remove FBFemale1-dilate40u-maxDist8u.surf FruMARCM-M002116_seg001_r4_01.nrrd FBFemale1-faces25k.surf Python SurfaceView2 Isosurface

# Create viewers
viewer setVertical 0

viewer 0 setBackgroundMode 1
viewer 0 setBackgroundColor 0 0.0980392 0.298039
viewer 0 setBackgroundColor2 0.686275 0.701961 0.807843
viewer 0 setTransparencyType 5
viewer 0 setAutoRedraw 0
viewer 0 show
mainWindow show

set hideNewModules 0
[ load ${SCRIPTDIR}/FBFemale1-dilate40u-maxDist8u.surf ] setLabel FBFemale1-dilate40u-maxDist8u.surf
FBFemale1-dilate40u-maxDist8u.surf setIconPosition 20 10
FBFemale1-dilate40u-maxDist8u.surf fire
FBFemale1-dilate40u-maxDist8u.surf LevelOfDetail setMinMax -1 -1
FBFemale1-dilate40u-maxDist8u.surf LevelOfDetail setButtons 1
FBFemale1-dilate40u-maxDist8u.surf LevelOfDetail setIncrement 1
FBFemale1-dilate40u-maxDist8u.surf LevelOfDetail setValue -1
FBFemale1-dilate40u-maxDist8u.surf LevelOfDetail setSubMinMax -1 -1
FBFemale1-dilate40u-maxDist8u.surf fire
FBFemale1-dilate40u-maxDist8u.surf setViewerMask 16383

set hideNewModules 0
[ load ${SCRIPTDIR}/../data/FruMARCM-M002116_seg001_r4_01.nrrd ] setLabel FruMARCM-M002116_seg001_r4_01.nrrd
FruMARCM-M002116_seg001_r4_01.nrrd setIconPosition 20 40
FruMARCM-M002116_seg001_r4_01.nrrd fire
FruMARCM-M002116_seg001_r4_01.nrrd setViewerMask 16383

set hideNewModules 0
[ load ${SCRIPTDIR}/FruMARCM-M002116_seg001-GHTZScale-v1-files/FBFemale1-faces25k.surf ] setLabel FBFemale1-faces25k.surf
FBFemale1-faces25k.surf setIconPosition 7 118
FBFemale1-faces25k.surf fire
FBFemale1-faces25k.surf LevelOfDetail setMinMax -1 -1
FBFemale1-faces25k.surf LevelOfDetail setButtons 1
FBFemale1-faces25k.surf LevelOfDetail setIncrement 1
FBFemale1-faces25k.surf LevelOfDetail setValue -1
FBFemale1-faces25k.surf LevelOfDetail setSubMinMax -1 -1
FBFemale1-faces25k.surf setTransform 1.09581 -0.09587 6.92387e-09 0 0.0955068 1.09164 -0.09587 0 0.006837 0.078142 0.896575 0 -76.591 -28.6926 -11.2776 1
FBFemale1-faces25k.surf fire
FBFemale1-faces25k.surf setViewerMask 16383

set hideNewModules 0
create HxPython {Python}
Python setIconPosition 309 70
Python fire
Python setViewerMask 16383
Python setPickable 1

set hideNewModules 0
create HxDisplaySurface {SurfaceView2}
SurfaceView2 setIconPosition 239 121
SurfaceView2 data connect FBFemale1-faces25k.surf
SurfaceView2 colormap setDefaultColor 1 0.1 0.1
SurfaceView2 colormap setDefaultAlpha 0.500000
SurfaceView2 colormap setLocalRange 0
SurfaceView2 fire
SurfaceView2 drawStyle setValue 4
SurfaceView2 fire
SurfaceView2 drawStyle setSpecularLighting 1
SurfaceView2 drawStyle setTexture 1
SurfaceView2 drawStyle setAlphaMode 3
SurfaceView2 drawStyle setNormalBinding 0
SurfaceView2 drawStyle setSortingMode 1
SurfaceView2 drawStyle setLineWidth 0.000000
SurfaceView2 drawStyle setOutlineColor 0 0 0.2
SurfaceView2 textureWrap setIndex 0 1
SurfaceView2 cullingMode setValue 0
SurfaceView2 selectionMode setIndex 0 0
SurfaceView2 Patch setMinMax 0 1
SurfaceView2 Patch setButtons 1
SurfaceView2 Patch setIncrement 1
SurfaceView2 Patch setValue 0
SurfaceView2 Patch setSubMinMax 0 1
SurfaceView2 BoundaryId setIndex 0 0
SurfaceView2 materials setIndex 0 1
SurfaceView2 materials setIndex 1 0
SurfaceView2 colorMode setIndex 0 0
SurfaceView2 baseTrans setMinMax 0 1
SurfaceView2 baseTrans setButtons 0
SurfaceView2 baseTrans setIncrement 0.1
SurfaceView2 baseTrans setValue 0.454545
SurfaceView2 baseTrans setSubMinMax 0 1
SurfaceView2 VRMode setIndex 0 0
SurfaceView2 fire
SurfaceView2 hideBox 1
{SurfaceView2} selectTriangles zab HIJMONMBABABAAAAAIACKAPOJPDGAPPIAAEIAAAAAALGKLAHONPBCIIB
SurfaceView2 fire
SurfaceView2 setViewerMask 16383
SurfaceView2 select
SurfaceView2 setShadowStyle 0
SurfaceView2 setPickable 1

set hideNewModules 0
create HxIsosurface {Isosurface}
Isosurface setIconPosition 170 80
Isosurface data connect FruMARCM-M002116_seg001_r4_01.nrrd
Isosurface colormap setDefaultColor 1 0.8 0.4
Isosurface colormap setDefaultAlpha 0.500000
Isosurface colormap setLocalRange 0
Isosurface fire
Isosurface drawStyle setValue 1
Isosurface fire
Isosurface drawStyle setSpecularLighting 1
Isosurface drawStyle setTexture 0
Isosurface drawStyle setAlphaMode 1
Isosurface drawStyle setNormalBinding 1
Isosurface drawStyle setSortingMode 1
Isosurface drawStyle setLineWidth 0.000000
Isosurface drawStyle setOutlineColor 0 0 0.2
Isosurface textureWrap setIndex 0 1
Isosurface cullingMode setValue 0
Isosurface threshold setMinMax 0 4242
Isosurface threshold setButtons 0
Isosurface threshold setIncrement 282.8
Isosurface threshold setValue 400
Isosurface threshold setSubMinMax 0 4242
Isosurface options setValue 0 1
Isosurface options setValue 1 0
Isosurface resolution setMinMax 0 -2147483648 2147483648
Isosurface resolution setValue 0 2
Isosurface resolution setMinMax 1 -2147483648 2147483648
Isosurface resolution setValue 1 2
Isosurface resolution setMinMax 2 -2147483648 2147483648
Isosurface resolution setValue 2 2
Isosurface numberOfCores setMinMax 1 10
Isosurface numberOfCores setButtons 1
Isosurface numberOfCores setIncrement 1
Isosurface numberOfCores setValue 2
Isosurface numberOfCores setSubMinMax 1 10
{Isosurface} doIt hit
Isosurface fire
Isosurface setViewerMask 16383
Isosurface setShadowStyle 0
Isosurface setPickable 1

set hideNewModules 0


viewer 0 setCameraOrientation 0.995822 0.0586767 -0.0699685 3.59227
viewer 0 setCameraPosition 145.772 444.554 -462.285
viewer 0 setCameraFocalDistance 653.198
viewer 0 setCameraNearDistance 408.452
viewer 0 setCameraFarDistance 806.158
viewer 0 setCameraType perspective
viewer 0 setCameraHeightAngle 44.9023
viewer 0 setAutoRedraw 1
viewer 0 redraw

