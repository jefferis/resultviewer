RootDir <- "~/projects/AnalysisSuite"
source(file.path(RootDir,"R/Code/Startup.R"))

config <- list(rootdir="~/2011/2011-07-29_InitBrainPosGHT")
config$srcdir <- file.path(config$rootdir, "src")
config$transform.path <- function(key) sprintf('%s/data/transforms/%s.txt', config$rootdir, key)
config$regdir <- '/Volumes/teraraid/prohaska/projects/TestReg'
config$reg.affine.relpath <- function(key) sprintf('Registration/affine/FC6475_%s_01_9dof.list', key)
config$reg.ght.relpath <- function(key) sprintf('Registration/ght/FC6475_%s_01_9dof.list', key)

setwd(config$rootdir)
